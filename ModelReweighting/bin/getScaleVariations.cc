#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Event.h"

#include "Core/CommonTools/interface/variables.h"
#include "Core/CommonTools/interface/toolbox.h"
#include "Core/CommonTools/interface/Looper.h"
#include "Core/CommonTools/interface/MetaInfo.h"

#include <TROOT.h>
#include <TString.h>
#include <TFile.h>
#include <TTreeReader.h>
#include <TH1D.h>
#include <TH2D.h>

#include "Math/VectorUtil.h"

using namespace std;
using namespace DAS;

////////////////////////////////////////////////////////////////////////////////
/// Template for function
void getScaleVariations 
             (TString input,  //!< name of input root file 
              TString output, //!< name of output root file
              /* TODO: add all arguments you may need */
              int nSplit = 1, //!< number of jobs/tasks/cores
              int nNow = 0)   //!< index of job/task/core
{
    TFile * source = TFile::Open(input, "READ");
    TTree * tree = dynamic_cast<TTree *>(source->Get("inclusive_jets"));

    MetaInfo metainfo(tree);
    if (nNow == 0) metainfo.Print();
    bool isMC = metainfo.isMC();

    TTreeReader reader(tree);
    //TTreeReaderValue<Event> ev = {reader, "event"};
    //TTreeReaderValue<PileUp> pu = {reader, "pileup"};
    //TTreeReaderValue<MET> met = {reader, "met"};
    //TTreeReaderValue<PrimaryVertex> pv = {reader, "primaryvertex"};
    TTreeReaderValue<vector<RecJet>> recjets = {reader, "recJets"};
    TTreeReaderValue<vector<GenJet>> * genjets = nullptr;
    if (isMC)
        genjets = new TTreeReaderValue<vector<GenJet>>({reader, "genJets"}); 
    // TODO: load any branch you may need

    TFile * file = TFile::Open(output, "RECREATE");

    // TODO: declare whatever histogram(s) you want to fill

    Looper looper(__func__, &reader, nSplit, nNow);
    while (looper.Next()) {

        // TODO: fill out histogram(s)

    }
    file->Write();
    file->Close();
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    TH1::SetDefaultSumw2();
    gROOT->SetBatch();

    if (argc < 3) {
        cout << argv[0] << " input output [nSplit [nNow]]" << endl;
        return EXIT_SUCCESS;
    }

    TString input = argv[1],
            output = argv[2];
    int nNow = 0, nSplit = 1;
    if (argc > 3) nSplit = atoi(argv[3]);
    if (argc > 4) nNow = atoi(argv[4]);

    getScaleVariations(input, output, nSplit, nNow);
    return EXIT_SUCCESS;
}
#endif
