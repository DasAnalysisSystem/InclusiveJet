# Model Dependence

Different approaches:
 - Take difference when using different samples to perform the unfolding (poor man's approach, but sometimes, there is no other choice).
 - Reweight simulation to data (risk to bias the measurement, should a priori be used only to derive an uncertainty, but not for the nominal value -- this is an approach defined for the inclusive jet analysis).
 - Vary model parameters (not implemented, but one could visit [this TWiki](https://twiki.cern.ch/twiki/bin/viewauth/CMS/HowToPDF) and [this repo](https://github.com/lviliani/LHEWeightsReader/blob/master/plugins/LHEWeightsDumper.cc)).
