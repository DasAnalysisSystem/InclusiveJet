#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Event.h"

#include "Core/CommonTools/interface/variables.h"
#include "Core/CommonTools/interface/toolbox.h"
#include "Core/CommonTools/interface/Greta.h"
#include "Core/CommonTools/interface/terminal.h"

#include <TROOT.h>
#include <TString.h>
#include <TFile.h>
#include <TF1.h>
#include <TH1.h>
#include <TH2.h>
#include <TH3.h>

#include "Math/VectorUtil.h"

using namespace std;
using namespace DAS;

////////////////////////////////////////////////////////////////////////////////
/// Determine reweighting functions that can be applied to the data in order
/// to remove statistical fluctuations.
///
/// The input spectrum is assumed to be normalised (i.e. don't apply it before 
/// combining the different triggers / applying the prescales).
void getSmoothData 
             (TString input,  //!< name of input root file (from `getSpectrum`)
              TString output) //!< name of output root file (simple root file)
{
    TFile * source = TFile::Open(input, "READ");
    TFile * file = TFile::Open(output, "RECREATE");

    for (int N = 1; N <= maxMult; ++N) { // N = jet mutiplicity
        TString hname = Form("pt_y_nth_Nbin%d", N);
        cout << hname << endl;

        source->cd();
        TH3 * hData3 = dynamic_cast<TH3*>(source->Get(hname));
        assert(hData3 != nullptr);
        hData3->SetDirectory(0);

        file->cd();
        TDirectory * d = file->mkdir(Form("Nbin%d", N));
        d->cd();
        hData3->SetDirectory(d);
        hData3->Write("h3");

        for (int y = 1; y <= nYbins; ++y) {

            TDirectory * dd = d->mkdir(Form("ybin%d", y));

            for (int n = 1; n <= N; ++n) { // n-th jet
                dd->cd();
                TDirectory * ddd = dd->mkdir(Form("nbin%d", n));
                ddd->cd();

                TH1 * h = hData3->ProjectionX("h", y, y, n, n);
                h->SetDirectory(ddd);
                h->SetTitle("#sigma");
                h->Scale(1,"width");
                h->Write("sigma");
                //h->Print("all");

                //static const vector<float> maxpts { 3103, 2787, 2500, 1784, 1101};
                int minpt = h->FindBin(74), // TODO: check
                    maxpt = h->FindBin(3450 /*maxpts.at(y-1)*/);
                const int degree = 7;
                while (abs(h->GetBinContent(minpt)) < deps && minpt + degree + 1 < maxpt) ++minpt;
                while (abs(h->GetBinContent(maxpt)) < deps && minpt + degree + 1 < maxpt) --maxpt;

                if (minpt + degree + 1 == maxpt) continue;

                auto f = GetSmoothFit<degree>(h, minpt, maxpt, 1, "Q0SNRE", true); // TODO: integral?

                // output
                if (f == nullptr) cout << red << "Failed: ";
                else              cout << green << "Success: ";
                cout << N << ' ' << y << ' ' << n << '\n' << normal << endl;

                if (f == nullptr) continue;
                TString fname = Form("pt_ybin%d_nth%d_Nbin%d", y, n, N);
                f->SetTitle(fname);
                f->Write("f");

                for (int i = 1; i <= h->GetNbinsX(); ++i) {
                    h->SetBinError  (i,0);
                    if (i < minpt || i >= maxpt) {
                        h->SetBinContent(i,0);
                        continue;
                    }

                    //double m = h->GetBinLowEdge(i),
                    //       M = h->GetBinLowEdge(i+1);
                    //double num = f->Integral(m, M);
                    double c = h->GetBinCenter(i);
                    double num = f->Eval(c);
                    double den = h->GetBinContent(i);
                    cout << i << setw(15) << c << setw(15) << num << setw(15) << den << setw(15) << (num/den) << endl;
                    if (den > 0) h->SetBinContent(i, num/den);
                }
                h->SetTitle("smoothing");
                h->Write("weight");
            }
        }
    }

    file->Write();
    file->Close();

    source->Close();
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    TH1::SetDefaultSumw2();
    gROOT->SetBatch();

    if (argc < 3) {
        cout << argv[0] << " input output [nSplit [nNow]]\n"
             << "\twhere\tinput = root file from `getSpectrum`\n"
             << "\t     \toutput = root file with reweighting functions to use in `applySmoothData`\n"
             << flush;
        return EXIT_SUCCESS;
    }

    TString input = argv[1],
            output = argv[2];

    getSmoothData(input, output);
    return EXIT_SUCCESS;
}
#endif
