#ifndef _matching_h_
#define _matching_h_

#include "Core/Objects/interface/Jet.h"

#include <iostream>
#include <vector>

#include "Math/VectorUtil.h"

using ROOT::Math::VectorUtil::DeltaR;

using namespace std;

namespace DAS::InclusiveJet {

static const FourVector NoMatch(1e5, 1e5, 1e5, 1e5); // for miss/fake entries

////////////////////////////////////////////////////////////////////////////////
/// Functor to perform the matching for the building of the RM
class Matching {

    double DR;

public:

    vector<GenJet> mcands; //!< matching candidates

#ifndef DOXYGEN_SHOULD_SKIP_THIS
    // dirty trick from Rakek...
    void Init(vector<GenJet> genJets, double dR = 0.2)
    {
        DR = dR;
        mcands = genJets;
    }
#endif

    ////////////////////////////////////////////////////////////////////////////////
    /// Constructor
    Matching(vector<GenJet> genJets, double dR = 0.2) : //!< list of generated jets (no double matching)
        DR(dR), mcands(genJets)
    {
    }
    Matching() {}

    ////////////////////////////////////////////////////////////////////////////////
    /// Operator to get the matched generated jet
    GenJet operator() (
            RecJet& recjet) //!< reconstructed jet
    {
        GenJet matchJet;
        matchJet.p4 = NoMatch; // by default no match

        for(auto it = mcands.begin(); it != mcands.end(); ++it) {
            if (DeltaR(it->p4, recjet.p4) > DR) continue;

            matchJet = *it;
            mcands.erase(it);
            break;
        }
        return matchJet;
    }
};

}
#endif
