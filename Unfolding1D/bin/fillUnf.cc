#include <cstdlib>
#include <cassert>
#include <iostream>

#include "Core/CommonTools/interface/variables.h"
#include "Core/CommonTools/interface/toolbox.h"
#include "Core/CommonTools/interface/Looper.h"
#include "Core/CommonTools/interface/MetaInfo.h"

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Event.h"

#include <TString.h>
#include <TFile.h>
#include <TTreeReader.h>
#include <TH1D.h>
#include <TH2D.h>

#include "matching.h"

using namespace std;
using namespace DAS;
using namespace DAS::InclusiveJet;

struct Variation {
    int iEvWgt, iJetWgt, iJEC;
};

const vector<int> nBinsY = {25, 24, 23, 20, 17};
static const int Ny = nBinsY.size();

void fillUnf (TString input, TString output, int nSplit = 1, int nNow = 0)
{
    TFile * source = TFile::Open(input, "READ");
    TTree * tree = dynamic_cast<TTree *>(source->Get("inclusive_jets"));

    MetaInfo metainfo(tree);
    metainfo.Print();
    bool isMC = metainfo.isMC();

    //auto variations = PrepareVariations(metainfo);

    TTreeReader reader(tree);
    //TTreeReader * reader = OpenNtuple(input);

    TTreeReaderValue<Event> event = {reader, "event"};
    TTreeReaderValue<vector<RecJet>> recjets = {reader, "recJets"};
    TTreeReaderValue<MET> met = {reader, "met"};
    TTreeReaderValue<PrimaryVertex> vtx = {reader, "primaryvertex"};
    TTreeReaderValue<vector<GenJet>> * genjets = nullptr;
    if (isMC) 
        genjets = new TTreeReaderValue<vector<GenJet>>({reader, "genJets"});

    TFile *file = TFile::Open(output, "RECREATE");
    //MetaInfo metainfo(tree);
    //metainfo.Print();
    //bool isMC = metainfo.isMC();


    // get number of variations and prepare sets of indices
    reader.SetEntry(1);
    int nEvWgt  = event->weights.size();
    int nJetWgt = recjets->front().weights.size();
    int nJEC    = recjets->front().JECs   .size();

    // Init vector with variations
    vector<Variation> sysVec;
    sysVec.push_back({0,0,0});
    for(int i = 1; i < nEvWgt; ++i) sysVec.push_back({i,0,0});
    for(int i = 1; i < nJetWgt;++i) sysVec.push_back({0,i,0});
    for(int i = 1; i < nJEC;   ++i) sysVec.push_back({0,0,i});
    const int nSys = sysVec.size();

    //Init Histograms
    vector<vector<TH1D*>>  hPtGen, hPtRec;
    vector<vector<TH2D*>>  hPtRM, hPtCov;

    vector<TH2*> hPtRecEta;

    hPtGen.resize(Ny);
    hPtRec.resize(Ny);
    hPtRM.resize(Ny);
    hPtCov.resize(Ny);
    hPtRecEta.resize(Ny);

    for(int y = 0; y < Ny; ++y) {
        hPtGen[y].resize(nSys);
        hPtRec[y].resize(nSys);
        hPtRM[y].resize(nSys);
        hPtCov[y].resize(nSys);

        int nGen = nBinsY[y];
        int nRec = 2*nBinsY[y];

        for(int isys = 0; isys < nSys; ++isys) {
            hPtGen[y][isys]  = new TH1D(Form("ptGenY%d_%d",y, isys), Form("ptGenY%d",y), nGen, genBins.data()); //TODO merge bins?
            hPtRec[y][isys]  = new TH1D(Form("ptRecY%d_%d",y, isys), Form("ptRecY%d",y), nRec, recBins.data()); 
            hPtRM[y][isys]   = new TH2D(Form("ptRMY%d_%d",y, isys),  Form("ptRMY%d",y), nGen, genBins.data(), 
                                                                                        nRec, recBins.data());
            hPtCov[y][isys]  = new TH2D(Form("ptCovY%d_%d",y, isys),  Form("ptCovY%d",y), nRec, recBins.data(), 
                                                                                          nRec, recBins.data());
        }

        hPtRecEta[y] = new TH2D(Form("ptRecEtaY%d", y), Form("ptRecEtaY%d", y), nRec, recBins.data(), 30, -3.0, 3.0);
    }

    Looper looper(__func__, &reader, nSplit, nNow);
    while (looper.Next()) {
        //Loop over systematics
        for (int isys = 0; isys < nSys; ++isys) {
            const Variation &var =  sysVec[isys];
            double evWgt = event->weights[var.iEvWgt];

            // Fill matched and fake rec jets
            Matching matching;
            if(isMC) matching.Init(*(*genjets));
            for (auto recjet: *recjets) {

                // reconstructed jet
                auto rec = recjet.p4;
                float n = recjet.JECs[var.iJEC];
                rec.SetPt(n*rec.Pt());

                // entry weight
                double genWgt = evWgt;
                double recWgt = evWgt*recjet.weights[var.iJetWgt];


                double rPt = rec.Pt();
                double Eta = rec.Eta();

                if(abs(vtx->z) > 24)           rPt = -1;

                double absRapRec = abs(Rapidity(rec));
                for(int y = 0; y < y_edges.size()-1; ++y) {
                    if(y_edges[y] <= absRapRec && absRapRec < y_edges[y+1]) {
                        hPtRec[y][isys]->Fill(rPt, recWgt);
                        if (isys == 0) 
                            hPtRecEta[y]->Fill(rPt, Eta, recWgt);
                    }
                    else
                        hPtRec[y][isys]->Fill(-1, recWgt);
                }


                //Cov matrix for both MC and data
                for(int y = 0; y < y_edges.size()-1; ++y) {
                    double rPtN = rPt;
                    if(absRapRec <  y_edges[y] ||  absRapRec >= y_edges[y+1])
                        rPtN = -1;

                    for (auto recjet2: *recjets) {
                        double recWgt2 = evWgt*recjet2.weights[var.iJetWgt];

                        auto rec2 = recjet2.p4;
                        float n = recjet2.JECs[var.iJEC];
                        rec2.SetPt(n*rec2.Pt());
                        double rPt2N = rec2.Pt();
                        if(rPtN == -1) rPt2N = -1;

                        double absRapRec2 = abs(Rapidity(rec2));
                        if(absRapRec2 <  y_edges[y] ||  absRapRec2 >= y_edges[y+1])
                            rPt2N = -1;

                        hPtCov[y][isys]->Fill(rPtN, rPt2N, recWgt*recWgt2);
                    }
                }



                //Only for MC
                if(!isMC) continue;

                // generated jet
                GenJet genjet = matching(recjet);
                auto gen = genjet.p4;

                double gPt = gen.Pt();
                double absRapGen = abs(Rapidity(gen));

                for(int y = 0; y < y_edges.size()-1; ++y) {
                    double rPtN = rPt, gPtN = gPt;
                    if(absRapRec <  y_edges[y] ||  absRapRec >= y_edges[y+1])
                        rPtN = -1;
                    if(absRapGen <  y_edges[y] ||  absRapGen >= y_edges[y+1])
                        gPtN = -1;

                    // filling
                    hPtGen[y][isys]->Fill(gPtN, genWgt);

                    // RM
                    hPtRM[y][isys]->Fill(gPtN, rPtN, recWgt);      //Reconstructed
                    hPtRM[y][isys]->Fill(gPtN, -1, genWgt-recWgt); //Not Reconstructed
                }

            }

            // Fill missed gen jets
            for (auto& genjet: matching.mcands) {
                double genWgt = evWgt;
                auto gen = genjet.p4;
                double gPt = gen.Pt();
                double absRapGen = abs(Rapidity(gen));
                for(int y = 0; y < y_edges.size()-1; ++y) {
                    double gPtN = gPt;
                    if(absRapGen <  y_edges[y] ||  absRapGen >= y_edges[y+1])
                        gPtN = -1;
                    hPtGen[y][isys]->Fill(gPtN, genWgt);
                    hPtRM[y][isys]->Fill(gPtN, -1.0, genWgt);//TODO Add genwgt
                }
            }
        }
    }
    file->Write();
    file->Close();
}

int main (int argc, char * argv[])
{
    TH1::SetDefaultSumw2();
    if (argc < 3) {
        cout << argv[0] << " input output [nSplit [nNow]]" << endl;
        return EXIT_FAILURE;
    }
    TString input  = argv[1],
            output = argv[2];
    int nNow = 0, nSplit = 1;
    if (argc > 3) nSplit = atoi(argv[3]);
    if (argc > 4) nNow = atoi(argv[4]);

    fillUnf(input, output, nSplit, nNow);
    return EXIT_SUCCESS;
}

