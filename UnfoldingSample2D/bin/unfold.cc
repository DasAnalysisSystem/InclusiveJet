#include <cassert>
#include <vector>
#include <cstdlib>

#include <TH1.h>
#include <TFile.h>
#include <TUnfold/TUnfoldBinning.h>
#include <TUnfold/TUnfoldDensity.h>
#include <TKey.h>

#include "Core/JEC/interface/JESreader.h"

#include "InclusiveJet/UnfoldingSample2D/interface/UnfHist.h"
#include "InclusiveJet/UnfoldingSample2D/interface/MyTUnfoldDensity.h"

#include "variation.h"

using namespace DAS;
using namespace DAS::InclusiveJet;

////////////////////////////////////////////////////////////////////////////////
/// Class unfolder performs the unfolding for one set of variations.
class Unfolder {

    UnfHist * Data, * MC;
    const TString name;

    ////////////////////////////////////////////////////////////////////////////////
    /// Get background from RM.
    ///
    /// *TODO*: check that it is non trivial.
    TH1 * GetBackground ()
    {
        TH1 * p = MC->RM->ProjectionX(Form("fake%d",rand()),0,0,"e");//summed over y-bins (events from outside)
        p->Divide(MC->RM->ProjectionX(Form("all%d",rand()),0,-1,"e"));//all events at det
        assert(Data->rec->GetNbinsX() == p->GetNbinsX());
        p->Multiply(Data->rec);

        return p;
    }

public:

    ////////////////////////////////////////////////////////////////////////////////
    /// Produce L matrix from MC with minimisation of the curvature of the surface.
    ///
    /// See also https://root.cern.ch/doc/master/TUnfold_8cxx_source.html#l01974
    void SetLMatrix (MyTUnfoldDensity * density)
    {
        TH2 * dummy = dynamic_cast<TH2*>(UnfHist::binsGen->ExtractHistogram("dummy", MC->gen));
        assert(dummy);
        int ptbins = dummy->GetNbinsX(),
             ybins = dummy->GetNbinsY();

        // getting global indices
        auto getGlobIndex = [dummy](int PtBin, int Ybin) {
            double pt = dummy->GetXaxis()->GetBinCenter(PtBin),
                   y  = dummy->GetYaxis()->GetBinCenter( Ybin);
            return UnfHist::binsGen->GetGlobalBinNumber(pt, y);
        };

        // getting values
        auto getVal = [this](int globalBin) {
            return 1./MC->gen->GetBinContent(globalBin);
        };

        // regularise bins
        for (int ptbin = 1; ptbin <= ptbins; ++ptbin)
        for (int  ybin = 1;  ybin <=  ybins; ++ ybin) {

            // check if one is not at a border
            bool bPtUp   = ptbin < ptbins,
                 bPtDown = ptbin > 1     ,
                 bYup    =  ybin <  ybins,
                 bYdown  =  ybin > 1     ;

            // global bin index (take only if not on a border)
            const Int_t gCenter = getGlobIndex(ptbin  , ybin  ),
                        gPtUp   = bPtUp   ? getGlobIndex(ptbin+1, ybin  ) : -1,
                        gPtDown = bPtDown ? getGlobIndex(ptbin-1, ybin  ) : -1,
                        gYup    = bYup    ? getGlobIndex(ptbin  , ybin+1) : -1,
                        gYdown  = bYdown  ? getGlobIndex(ptbin  , ybin-1) : -1;

            // values (curvature regularisation)
            const Double_t vPtUp   = bPtUp   ? -getVal(gPtUp  ) : 0,
                           vPtDown = bPtDown ? -getVal(gPtDown) : 0,
                           vYup    = bYup    ? -getVal(gYup   ) : 0,
                           vYdown  = bYdown  ? -getVal(gYdown ) : 0,
                           vCenter = -(vPtUp+vPtDown+vYup+vYdown);
                  
            // cheating with call to protected method
            Int_t indices[5];
            Double_t rowData[5];
                           indices[0] = gCenter; rowData[0] = vCenter; int i = 1;
            if (bPtUp  ) { indices[i] = gPtUp  ; rowData[i] = vPtUp  ; ++i; }
            if (bPtDown) { indices[i] = gPtDown; rowData[i] = vPtDown; ++i; }
            if (bYup   ) { indices[i] = gYup   ; rowData[i] = vYup   ; ++i; }
            if (bYdown ) { indices[i] = gYdown ; rowData[i] = vYdown ; ++i; }
            density->MyAddRegularisationCondition(i, indices, rowData);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Constructor
    Unfolder
        (UnfHist * Data_, //!< data histograms
         UnfHist * MC_,   //!< MC histograms
         TString Name) :  //!< variation name
        Data(Data_), MC(MC_), name(Name)
    {
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Perform the unfolding with the bin-by-bin method.
    TH1 * binbybin ()
    {
        TString name = MC->name + Form("%d", rand());
        TH1 * h = dynamic_cast<TH1*>(Data->recMerged->Clone(name));
        h->Divide(dynamic_cast<TH1*>(MC->recMerged));
        h->Multiply(dynamic_cast<TH1*>(MC->gen));
        return UnfHist::binsGen->ExtractHistogram(MC->name + "bbb", h);
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Perform the (pseudo) inversion with Tikhonov regularisation.
    TH1 * inversion ()
    {
        // TUnfoldDensity (const TH2 *hist_A,
        //                  EHistMap histmap,
        //                  ERegMode regmode=kRegModeCurvature,
        //                  EConstraint constraint=kEConstraintArea,
        //                  EDensityMode densityMode=kDensityModeBinWidthAndUser,
        //                  const TUnfoldBinning *outputBins=0,
        //                  const TUnfoldBinning *inputBins=0,
        //                  const char *regularisationDistribution=0,
        //                  const char *regularisationAxisSteering="*[UOB]")

        //Clone to not change original histograms
        cout << "Name " << name << endl;
        assert(MC->RM);
        MC->RM->GetBinContent(1,1);
        TH2 *hRM = nullptr;
        TH1 *hIn = nullptr;
        TString nameNow = name;
        if (name == "bgUp" || name == "bgDown")
            nameNow = "nominal";
        hRM = dynamic_cast<TH2*>(MC->RM->Clone(Form("%s%d",nameNow.Data(),rand())));
        hIn = dynamic_cast<TH1*>(Data->rec->Clone(Form("%s%d",nameNow.Data(),rand())));

        MyTUnfoldDensity * density = new MyTUnfoldDensity(hRM,
                                                      TUnfold::kHistMapOutputVert,
                                                      TUnfold::kRegModeNone,
                                                      TUnfold::kEConstraintNone,
                                                      TUnfoldDensity::kDensityModeBinWidthAndUser,
                                                      UnfHist::binsGen,
                                                      UnfHist::binsRec);
        density->SetEpsMatrix(numeric_limits<double>::epsilon());

        cout << "Getting (and subtracting) background" << endl;
        TH1 * bgr = GetBackground();
        //Erase fakes in RM (we treat them using bgr)
        //for(int i = 0; i <= hRM->GetNbinsX(); ++i) {
        //    double content = hRM->GetBinContent(i, 0);
        //    if (content > 0)
        //        cout << i << '\t' << content << '\n';
        //}
        //cout << endl;
        //    hRM->SetBinContent(i, 0);
        //Substract BG from input data
        static const float bg_var = 0.1;
        if (name == "bgUp")
            hIn->Add(bgr, -(1.0+bg_var));
        else if (name == "bgDown")
            hIn->Add(bgr, -(1.0       ));
        else
            hIn->Add(bgr, -(1.0-bg_var));

        cout << "Setting input " << endl;
        {
            double scaleBias = 0, oneOverZeroError = 0;
            int status = density->SetInput(hIn, scaleBias, oneOverZeroError, Data->cov);
            int nBadErrors = status%10000,
                nUnconstrOutBins = status/10000;
            cout << nBadErrors << " bad errors and " << nUnconstrOutBins << " unconstrained output bins" << endl;
        }

        cout << "Unfolding" << endl;
        density->DoUnfold(0.); // no Tikhonov regularisation

        //SetLMatrix(density); // TODO: re-use the same L-matrix for each variation
        //static double tau = 0;
        //if (name == "nominal") {

        //    // L-curve scan
        //    TGraph * lcurve = nullptr;
        //    density->ScanLcurve(100, 1e-5,1, &lcurve);
        //    tau = density->GetTau();
        //    lcurve->Write();

        //    density->DoUnfold(tau);

        //    // regularisation uncertainty
        //    density->SetTauError(tau/2);
        //    TH1 * h_systau = density->GetDeltaSysTau("SysTau", "Regularisation uncertainty");
        //    assert(h_systau);
        //    h_systau->Write();
        //    
        //    // RM uncertainty
        //    TH2 * h_MCunc = density->GetEmatrixSysUncorr("RMstat", "RM statistical uncertainty");
        //    assert(h_MCunc);
        //    h_MCunc->Write();

        //    //for (int i = 0; i <= 10; ++i) {
        //    //    double t = pow(10,-i/2.);
        //    //    density->DoUnfold(t);
        //    //    TString name = Form("scan%d", i), title = Form("#tau = %f", t);
        //    //    cout << title << endl;
        //    //    TH1 * h = density->GetOutput(name);
        //    //    h->SetTitle(title);
        //    //    h->Write();
        //    //}
        //}
        //else {
        //    assert(tau != 0);
        //    density->DoUnfold(tau); // argument corresponds to tau parameter
        //}

        cout << "Returning output" << endl;
        TH1 * output = density->GetOutput(name);
        assert(output);
        return output;
    }
};

////////////////////////////////////////////////////////////////////////////////
/// Get systematic shifts based on the information in the meta information.
vector<TString> getSysShifts(TFile *file)
{
    vector<TString> systematics;

    TList* List = file->GetListOfKeys() ;
    //List->Print();
    TIter next(List) ;
    TKey* key ;

    //cout << "\tLooping over keys in file" << endl;
    while ( (key = (TKey*)next()) ) {
        TObject * obj = key->ReadObj() ;
        TString name = obj->GetName();
        systematics.push_back(name);
    }

    return systematics;
}

////////////////////////////////////////////////////////////////////////////////
/// Main routine to unfold.
void unfold 
        (TString input,   //!< name of input root file with data unfolding histograms
         TString output,  //!< name of output root file with unfolded data histograms
         TString RM)      //!< name of input root file with RM 
{
    cout << "Using following TUnfold version: " << TUnfold::GetTUnfoldVersion() << endl;

    vector<TString> orderedVariations {"nominal", "JERup", "JERdown", "PUup", "PUdown", "Prefup", "Prefdown"/*, "bgUp", "bgDown"*/};
    for (TString& JES: JESreader::getJECUncNames()) {
        orderedVariations.push_back(JES + "up");
        orderedVariations.push_back(JES + "down");
    }

    map<TString,UnfHist *>  variationsData, variationsMC;

    // TODO: simplify use of `getSysShifts`

    cout << "\e[1mGetting data variations\e[0m" << endl;
    TFile * fileData = TFile::Open(input, "READ");
    for (auto s: getSysShifts(fileData) ) {
        cout << s << endl;
        variationsData[s] = new UnfHist("Data", dynamic_cast<TDirectory*>(fileData->Get(s)));
    }

    cout << "\e[1mGetting MC variations\e[0m" << endl;
    TFile * fileRM = TFile::Open(RM, "READ");
    for (auto s: getSysShifts(fileRM) ) {
        variationsMC[s] = new UnfHist("MC", dynamic_cast<TDirectory*>(fileRM->Get(s)));
    }

    cout << "\e[1mStarting the unfolding\e[0m" << endl;
    TFile * file = TFile::Open(output, "RECREATE");

    // systematic shifts from input
    for (TString name: orderedVariations) {
        cout << name <<  endl;

        UnfHist *dataVar = variationsData.count(name) > 0 ? variationsData.at(name) : variationsData.at("nominal");
        UnfHist *mcVar   = variationsMC  .count(name) > 0 ? variationsMC  .at(name) : variationsMC  .at("nominal");

        Unfolder unfolder(dataVar, mcVar, name);
        TH1 * h_inversion = unfolder.inversion();
        h_inversion->Write();

        if (name != "nominal") continue;

        cout << "Bin-by-bin" << endl;
        TH1 * h_bbb = unfolder.binbybin();
        h_bbb->Write();

        cout << "Lumi uncertainty" << endl;
        TH1 * h_lumiup = dynamic_cast<TH1*>(h_inversion->Clone("lumiup")),
            * h_lumidown = dynamic_cast<TH1*>(h_inversion->Clone("lumidown"));
        static const double lumiUnc = 0.02;
        h_lumiup->Scale(1+lumiUnc);
        h_lumidown->Scale(1-lumiUnc);
        h_lumiup->Write();
        h_lumidown->Write();
    }

    file->Close();
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    TH1::SetDefaultSumw2();

    if (argc < 4) {
        cout << argv[0] << "input output MC\n"
             << "\twhere\tinput = input data prepared spectrum at rec level\n"
             << "\t     \toutput = output with unfolding spectrum\n"
             << "\t     \tMC = input MC RMs" << endl;
        return EXIT_SUCCESS;
    }

    TString intput = argv[1],
            output = argv[2],
            MC = argv[3];

    unfold(intput, output, MC);

    return EXIT_SUCCESS;
}
#endif
