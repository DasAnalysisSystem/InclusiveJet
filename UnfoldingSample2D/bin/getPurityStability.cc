#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>
#include <numeric>

#include <TROOT.h>
#include <TString.h>
#include <TFile.h>
#include <TH1.h>
#include <TH2.h>
#include <TH3.h>

#include "Core/JEC/bin/common.h"
#include "Core/CommonTools/interface/variables.h"

#include "Math/VectorUtil.h"

using namespace std;
using namespace DAS;

////////////////////////////////////////////////////////////////////////////////
/// Just a small class to split functions into several functions with "common header".
/// Produces purity and stability for 2D inclusive jet measurement.
/// 
/// Technical note: 
/// - `TH3::Integral()` should be used for summing over ptgen, ptrec and ygen
/// - `std::accumulate()` combined with `TH3::SetBinContent` should be used for summing over yrec
struct RMprojections {

    vector<TH3 *> h_y;
    TH2 * h_fakeOut, * h_fakeNoM, * h_missOut, * h_missNoM;

    ////////////////////////////////////////////////////////////////////////////////
    /// Constructor: extract RMs and fake/miss rates
    RMprojections (TDirectory * source, const char * variation) :
        h_y(nYbins)
    {
        source->cd();
        for (int y = 1; y <= nYbins; ++y) {
            const char * name = Form("RM_%s_ybin%d", variation, y);
            h_y[y-1] = dynamic_cast<TH3 *>(source->Get(name));
            h_y[y-1]->SetDirectory(0);
        }

        h_fakeOut = dynamic_cast<TH2 *>(source->Get(Form("fakeOut_%s"    , variation)));
        h_fakeNoM = dynamic_cast<TH2 *>(source->Get(Form("fakeNoMatch_%s", variation)));
        h_missOut = dynamic_cast<TH2 *>(source->Get(Form("missOut_%s"    , variation)));
        h_missNoM = dynamic_cast<TH2 *>(source->Get(     "missNoMatch"               ));
        for (TH1 * h: { h_fakeOut, h_fakeNoM, h_missOut, h_missNoM }) 
            h->SetDirectory(0);
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Extract purity with gen-level binning
    void getPurity (int y)
    {
        TH1 * A = new TH1F(    "noMig", "no migration"                   , nGenBins, genBins.data()),
            * B = new TH1F(    "ptMig", "same y bin, different p_{T} bin", nGenBins, genBins.data()),
            * C = new TH1F(     "yMig", "same p_{T} bin, different y bin", nGenBins, genBins.data()),
            * D = new TH1F("doubleMig", "different p_{T} and y bins"     , nGenBins, genBins.data());

        for (int ptgen = 1; ptgen <= nGenBins; ++ptgen) {
            int ptrec1 = ptgen*2-1, ptrec2 = ptgen*2;
            float a =                                                                         h_y[y-1]->Integral(ptrec1, ptrec2, ptgen,    ptgen, y,      y),
                  b =                                                                         h_y[y-1]->Integral(ptrec1, ptrec2,     1, nGenBins, y,      y)     - a,
                  c =                                                                         h_y[y-1]->Integral(ptrec1, ptrec2, ptgen,    ptgen, 1, nYbins)     - a,
                  d = accumulate(h_y.begin(), h_y.end(), 0.f, [&](float sum, TH3 * h) { return sum + h->Integral(ptrec1, ptrec2,     1, nGenBins, y,      y); }) - a - b - c,
                  fn=                                                                        h_fakeNoM->Integral(ptrec1, ptrec2,                  y,      y),
                  fo=                                                                        h_fakeOut->Integral(ptrec1, ptrec2,                  y,      y),
                  s = a + b + c + d + fn + fo;

            if (s < 1e-20) continue;

            for (float * v: {&a, &b, &c, &d}) *v /= s;

            A->SetBinContent(ptgen,a);
            B->SetBinContent(ptgen,b);
            C->SetBinContent(ptgen,c);
            D->SetBinContent(ptgen,d);
        }

        for (TH1 * h: {A, B, C, D}) h->Write();
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Extract stability with gen-level binning
    void getStability (int y)
    {
        TH1 * A = new TH1F(    "noMig", "no migration"                   , nGenBins, genBins.data()),
            * B = new TH1F(    "ptMig", "same y bin, different p_{T} bin", nGenBins, genBins.data()),
            * C = new TH1F(     "yMig", "same p_{T} bin, different y bin", nGenBins, genBins.data()),
            * D = new TH1F("doubleMig", "different p_{T} and y bins"     , nGenBins, genBins.data());

        for (int ptgen = 1; ptgen <= nGenBins; ++ptgen) {
            int ptrec1 = ptgen*2-1, ptrec2 = ptgen*2;
            float a =                                                                         h_y[y-1]->Integral(ptrec1,   ptrec2, ptgen, ptgen, y, y),
                  b =                                                                         h_y[y-1]->Integral(     1, nRecBins, ptgen, ptgen, y, y)     - a,
                  c = accumulate(h_y.begin(), h_y.end(), 0.f, [&](float sum, TH3 * h) { return sum + h->Integral(ptrec1,   ptrec2, ptgen, ptgen, y, y); }) - a,
                  d = accumulate(h_y.begin(), h_y.end(), 0.f, [&](float sum, TH3 * h) { return sum + h->Integral(     1, nRecBins, ptgen, ptgen, y, y); }) - a - b - c,
                  mn=                                                                        h_missNoM->Integral(                  ptgen, ptgen, y, y),
                  mo=                                                                        h_missOut->Integral(                  ptgen, ptgen, y, y),
                  s = a + b + c + d + mn + mo;

            if (s < 1e-20) continue;

            for (float * v: {&a, &b, &c, &d}) *v /= s;

            A->SetBinContent(ptgen,a);
            B->SetBinContent(ptgen,b);
            C->SetBinContent(ptgen,c);
            D->SetBinContent(ptgen,d);
        }

        for (TH1 * h: {A, B, C, D}) h->Write();
    }
};

////////////////////////////////////////////////////////////////////////////////
/// Extract purity and stability from RMs in `getJERcurves`
void getPurityStability 
             (TString input,  //!< name of input root file 
              TString output) //!< name of output root file
{
    /// Opening source and checking that it is MC
    TFile * source = TFile::Open(input, "READ");
    TFile * file = TFile::Open(output, "RECREATE");

    for (const char * variation: variations) {

        file->cd();
        TDirectory * d_var = file->mkdir(variation);
        d_var->cd();

        RMprojections p(source, variation);

        // purity
        d_var->cd();
        TDirectory * dp = d_var->mkdir("purity");
        for (int y = 1; y <= nYbins; ++y) {
            dp->cd();
            TDirectory * dy = dp->mkdir(Form("ybin%d", y), yBins[y-1]);
            dy->cd();
            p.getPurity(y);
        }

        // stability
        d_var->cd();
        TDirectory * ds = d_var->mkdir("stability");
        for (int y = 1; y <= nYbins; ++y) {
            ds->cd();
            TDirectory * dy = ds->mkdir(Form("ybin%d", y), yBins[y-1]);
            dy->cd();
            p.getStability(y);
        }
    }

    file->Close();
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    TH1::SetDefaultSumw2();
    gROOT->SetBatch();

    if (argc < 3) {
        cout << argv[0] << " input output\n"
             << "\twhere\tinput = root file obtained after `getJERcurves`\n"
             << "\t     \toutput = root file containing purity and stability per variation of JER and per rapidity bin\n"
             << flush;
        return EXIT_SUCCESS;
    }

    TString input = argv[1],
            output = argv[2];

    getPurityStability(input, output);
    return EXIT_SUCCESS;
}
#endif

