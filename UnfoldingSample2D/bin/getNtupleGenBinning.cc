#include <cstdlib>
#include <cassert>
#include <iostream>
#include <algorithm>
#include <vector>

#include "Core/CommonTools/interface/toolbox.h"
#include "Core/CommonTools/interface/Looper.h"
#include "Core/CommonTools/interface/MetaInfo.h"

#include "InclusiveJet/UnfoldingSample2D/interface/UnfHist.h"

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Event.h"

#include <TString.h>
#include <TFile.h>
#include <TTreeReader.h>
#include <TH1D.h>
#include <TH2D.h>

#include "common.h"

#include "Math/GenVector/GenVector_exception.h"

using namespace std;
using namespace DAS;
using namespace DAS::InclusiveJet;

////////////////////////////////////////////////////////////////////////////////
/// Simply function to project the generated spectra from the *n*-tuples of 
/// the simulation onto histograms with the same binning as the unfolded spectra.
void getNtupleGenBinning 
            (TString input,      //!< name of input root file
			 TString output,     //!< name of output root file
             int nSplit = 1,     //!< number of jobs/tasks/cores
             int nNow = 0)       //!< index of job/task/core
{
    TFile * source = TFile::Open(input, "READ");
    TTree * tree = dynamic_cast<TTree *>(source->Get("inclusive_jets"));

    MetaInfo metainfo(tree);
    metainfo.Print();
    bool isMC = metainfo.isMC();

    TH2 * h_rec = new TH2D("rec", "", UnfHist::nPtBinsGen, UnfHist::gen_pt_edges.data(),
                                      UnfHist::nYbinsGen , UnfHist::gen_y_edges.data());
    TH2 * h_gen = nullptr;
    if (isMC) h_gen = dynamic_cast<TH2*>(h_rec->Clone("gen"));

    TTreeReader reader(tree);
    TTreeReaderValue<Event> event = {reader, "event"};
    TTreeReaderValue<vector<RecJet>> recjets = {reader, "recJets"};
    TTreeReaderValue<vector<GenJet>> * genjets = nullptr;
    if (isMC)
        genjets = new TTreeReaderValue<vector<GenJet>>({reader, "genJets"}); 

    TFile *file = TFile::Open(output, "RECREATE");

    Looper looper(__func__, &reader, nSplit, nNow);
    while (looper.Next()) {

        double weight = event->weights.front();

        for (auto& recjet: *recjets) 
            h_rec->Fill(recjet.CorrPt(), abs(Rapidity(recjet)), weight);

        if (!isMC) continue;

        for (auto& genjet: *(*genjets)) {
            const FourVector& p4 = genjet.p4;
            h_gen->Fill(p4.Pt(), abs(Rapidity(p4)), weight);
        }
    }

    SaveYbins({h_rec});
    if (h_gen) SaveYbins({h_gen});

    file->Close();
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    TH1::SetDefaultSumw2();

    if (argc < 3) {
        cout << argv[0] << " input output [nSplit [nNow]]\n"
             << "\twhere\tinput = n-tuple\n"
             << "\t     \toutput = file with spectra in the binning after unfolding" << endl;
        return EXIT_SUCCESS;
    }

    TString input = argv[1],
            output = argv[2];
    int nNow = 0, nSplit = 1;
    if (argc > 3) nSplit = atoi(argv[3]);
    if (argc > 4) nNow = atoi(argv[4]);

    getNtupleGenBinning(input, output, nSplit, nNow);
    return EXIT_SUCCESS;
}
#endif
