#include "Core/CommonTools/interface/toolbox.h"
#include "Core/Objects/interface/Jet.h"

#include "InclusiveJet/UnfoldingSample2D/interface/UnfHist.h"

#include <TUnfold/TUnfoldBinning.h>
#include <TH1.h>
#include <TFile.h>

#include <cstdlib>
#include <iostream>
#include <cassert>
#include <algorithm>
#include <limits>

constexpr const char * TUnfV = TUnfold_VERSION;
static_assert(strcmp(TUnfV, "V17.9") == 0);

using namespace std;
using namespace DAS;
using namespace DAS::InclusiveJet;

static const double eps = numeric_limits<double>::epsilon();

const vector<double> UnfHist::rec_pt_edges{74,84,97,114,133,153,174,196,220,245,272,300,330,362,395,430,468,507,548,592,638,686,737,790,846,905,967,1032,1101,1172,1248,1327,1410,1497,1588,1684,1784,1890,2000,2116,2238,2366,2500,2640,2787,2941,3103},
             UnfHist::gen_pt_edges{74,97,133,174,220,272,330,395,468,548,638,737,846,967,1101,1248,1410,1588,1784,2000,2238,2500,2787,3103},
             UnfHist::rec_y_edges{0.0,0.5,1.0,1.5,2.0,2.5},
             UnfHist::gen_y_edges{0.0,0.5,1.0,1.5,2.0,2.5};

const size_t UnfHist::nPtBinsGen = gen_pt_edges.size()-1,
             UnfHist::nPtBinsRec = rec_pt_edges.size()-1,
             UnfHist::nYbinsGen = gen_y_edges.size()-1,
             UnfHist::nYbinsRec = rec_y_edges.size()-1;

inline TUnfoldBinning * MakeBinning (TString type)
{
    TUnfoldBinning * binning = new TUnfoldBinning(type);
    binning->PrintStream(cout);

    if (type == "gen") {
        binning->AddAxis("pt", UnfHist::nPtBinsGen, UnfHist::gen_pt_edges.data(), false, true );
        binning->AddAxis("y" , UnfHist::nYbinsGen , UnfHist::gen_y_edges .data(), false, false);
    }
    else if (type == "rec") {
        binning->AddAxis("pt", UnfHist::nPtBinsRec, UnfHist::rec_pt_edges.data(), false, true );
        binning->AddAxis("y" , UnfHist::nYbinsRec , UnfHist::rec_y_edges .data(), false, false);
    }
    else {
        cerr << type << " is not reconised\n";
        exit(EXIT_FAILURE);
    }

    return binning;
}

const TUnfoldBinning * UnfHist::binsGen = MakeBinning("gen"),
                     * UnfHist::binsRec = MakeBinning("rec");

UnfHist::UnfHist (TString Name, bool MC) :
    name(Name)
{
    cout << "Using following TUnfold version: " << TUnfold::GetTUnfoldVersion() << endl; 

    gen = MC ? binsGen->CreateHistogram("gen_" + name) : nullptr;
    rec = binsRec->CreateHistogram("rec_" + name);
    recMerged = binsGen->CreateHistogram("recMerged_" + name); // just to make the bin-by-bin correction as a cross-check
    recBG = binsRec->CreateHistogram("recBG_" + name);
    RM = MC ? TUnfoldBinning::CreateHistogramOfMigrations(binsRec, binsGen, "RM_" + name) : nullptr;
    cov = binsRec->CreateErrorMatrixHistogram("cov_" + name, false);//isOrgBinning = false
}

void UnfHist::ScanRecBinning ()
{
    for (double y: rec_y_edges) 
    for (double pt: rec_pt_edges) {
        double BinID = binsRec->GetGlobalBinNumber(pt+1, y+0.1);
        cout << setw(5) << y << setw(5) << pt << setw(5) << BinID << '\n';
    }
    cout << endl;
}

UnfHist::UnfHist (const char * prefix, TDirectory * dir) :
    name(dir->GetName()),
    gen      (dynamic_cast<TH1*>(dir->Get("gen"      ))),
    rec      (dynamic_cast<TH1*>(dir->Get("rec"      ))),
    recMerged(dynamic_cast<TH1*>(dir->Get("recMerged"))),
    recBG    (dynamic_cast<TH1*>(dir->Get("recBG"    ))),
    RM       (dynamic_cast<TH2*>(dir->Get("RM"       ))),
    cov      (dynamic_cast<TH2*>(dir->Get("cov"      )))
{
    cout << "Using following TUnfold version: " << TUnfold::GetTUnfoldVersion() << endl; 

    assert(dir != nullptr);
    for (TH1 * h: { gen, rec, recMerged, recBG,
             dynamic_cast<TH1*>(RM), dynamic_cast<TH1*>(cov)} ) {
        if (h == nullptr) continue;
        h->SetName(Form("%s_%s_%s", prefix, h->GetName(), name.Data()));
        h->SetDirectory(0);
    }
}

void UnfHist::FillGen (const FourVector& jet, double weight)
{
    double pt = jet.Pt() , y = abs(Rapidity(jet));
    double BinID = binsGen->GetGlobalBinNumber(pt, y);

    if (BinID > 0)
        gen->Fill(BinID, weight);
}

void UnfHist::FillRec (const FourVector& jet, double weight)
{
    double pt = jet.Pt(), y = abs(Rapidity(jet));

    double BinID = binsRec->GetGlobalBinNumber(pt, y);
    if (BinID > 0)
        rec->Fill(BinID, weight);

    BinID = binsGen->GetGlobalBinNumber(pt, y);
    if (BinID > 0)
        recMerged->Fill(BinID, weight);
}

void UnfHist::FillRecBG (const FourVector& jet, double weight)
{
    double pt = jet.Pt(), y = abs(Rapidity(jet));

    double BinID = binsRec->GetGlobalBinNumber(pt, y);
    if (BinID > 0)
        recBG->Fill(BinID, weight);
}

void UnfHist::FillCov (const FourVector& jet1, const FourVector& jet2, double weight)
{
    assert(cov);

    double pt1 = jet1.Pt(), y1 = abs(Rapidity(jet1));
    double pt2 = jet2.Pt(), y2 = abs(Rapidity(jet2));

    double Bin1ID = binsRec->GetGlobalBinNumber(pt1, y1);
    double Bin2ID = binsRec->GetGlobalBinNumber(pt2, y2);

    if (Bin1ID > 0 && Bin1ID > 0)
        cov->Fill(Bin1ID, Bin2ID, weight);
}

void UnfHist::FillRM  (const FourVector& genjet, const FourVector& recjet,
                            double weightGen, double weightRec)
{
    double recpt = recjet.Pt(), yRec = abs(Rapidity(recjet));
    double genpt = genjet.Pt(), yGen = abs(Rapidity(genjet));

    double recBinID  = binsRec->GetGlobalBinNumber(recpt, yRec);
    double fakeBinID = binsRec->GetGlobalBinNumber(    1, yRec); // TODO: check
    double genBinID  = binsGen->GetGlobalBinNumber(genpt, yGen);

    if (recBinID > 0 && genBinID > 0) {
        RM->Fill(recBinID,  genBinID,             weightRec);
        RM->Fill(fakeBinID, genBinID, weightGen - weightRec); // TODO: check

        RMfine->Fill(recBinID,  recBinID,             weightRec);
        RMfine->Fill(fakeBinID, recBinID, weightGen - weightRec); // TODO: check
    }
}

void UnfHist::Print ()
{
    TH1 * Ratio = RM->ProjectionY();
    Ratio->Divide(gen);
    for (int i = 1; i <= Ratio->GetNbinsX(); ++i) {
        double content = Ratio->GetBinContent(i);
        if (abs(content) > eps && abs(content-1) > eps)
            Ratio->Print("all");
    }
    delete Ratio;
}

//vector<TH1 *> GetVectorGen ()
//{
//    // TODO
//}
//
//vector<TH1 *> GetVectorRec ()
//{
//    // TODO
//}
//
//vector<vector<TH2 *>> GetMatrixMigrations ()
//{
//    // TODO
//}

void UnfHist::Write (TFile * f)
{
    assert(f != nullptr);
    assert(f->IsOpen());
    TDirectory * dir = f->mkdir(name);
    dir->cd();
    if (gen       != nullptr) { gen      ->SetDirectory(dir); gen      ->Write("gen"      ); }
    if (rec       != nullptr) { rec      ->SetDirectory(dir); rec      ->Write("rec"      ); }
    if (recMerged != nullptr) { recMerged->SetDirectory(dir); recMerged->Write("recMerged"); }
    if (recBG     != nullptr) { recBG    ->SetDirectory(dir); recBG    ->Write("recBG"    ); }
    if (RM        != nullptr) { RM       ->SetDirectory(dir); RM       ->Write("RM"       ); }
    if (cov       != nullptr) { cov      ->SetDirectory(dir); cov      ->Write("cov"      ); }
    f->cd();
}
