#ifndef DOXYGEN_SHOULD_SKIP_THIS

#ifndef __LCURVE__
#define __LCURVE__

#include <TGraph.h>
#include <TSpline.h>
#include <TFile.h>

namespace DAS::InclusiveJet {

struct LcurveInfo {
    int iBest;
    double tau, chi2A, chi2L;
    int Ndf;

    LcurveInfo ();
    LcurveInfo (TFile * file);

    void Write ();
};

struct Lcurve {
    static const int nsteps;
    static const double mintau, maxtau, tauVariation;
    TGraph * lCurve;
    TSpline * logTauX, * logTauY/*, * logTauCurvature*/;
    Color_t c;
    LcurveInfo info;

    Lcurve (Color_t color = kBlack);
    Lcurve (TString filename, Color_t color);

    void DrawLcurve ();
    void DrawTauX ();
    void DrawTauY ();
};

}

#endif
#endif
