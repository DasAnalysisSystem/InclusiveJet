#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>
#include <algorithm>

#include <TROOT.h>
#include <TString.h>
#include <TFile.h>
#include <TH1.h>
#include <TH2.h>
#include <TH3.h>
#include <TF1.h>
#include <TF2.h>

#include "Math/VectorUtil.h"

#include "Core/CommonTools/interface/variables.h"
#include "Core/CommonTools/interface/toolbox.h"
#include "Core/CommonTools/interface/Greta.h"

#include "Core/JEC/bin/common.h"
#include "Core/JEC/interface/resolution.h"
#include "Core/JEC/interface/DoubleCrystalBall.h"

using namespace std;
using namespace DAS;

static const size_t nMiss = 4;

typedef ThunbergDoubleCrystalBall<nmu, nkL, nnL, nkR, nnR, DoubleCrystalBall::Integral> ResolutionInt2D;

struct Scenario {
    const char * name; //!< global name (used for output directory)
    const char * JERvariation;
    const double effScale; //!< miss
    const double backScale; //!< fake
    const double LHSk, LHSn, RHSk, RHSn;
    //const double kFactor, //!< tail turn-on modification factor
    //             nFactor; //!< tail exponent factor
};

////////////////////////////////////////////////////////////////////////////////
/// Toy
///
/// A priori, there is no strong requirement to have a functor or an object 
/// rather than a simple function. The goal is simply to structure a bit more 
/// the code, splitting declaration/preparation and actual calculation into
/// smaller pieces of code visible within a screen (~100 lines at most).
struct Toy {

    const Scenario& scenario;

    static TDirectory * input,
                      * output;

    template<typename T, typename ... Args> inline T * Get (Args ... args) { return dynamic_cast<T*>(input->Get(Form(args...))); }

    TDirectory * d; //!< output directory, where histograms are saved

    vector<Thunberg<nN>> gens;
    vector<Thunberg<nMiss>> missOuts, missNoMatches;
    vector<ResolutionInt2D> resolutions;

    vector<TH1 *> hGens, //!< original gen spectrum
                  hRecs; //!< original rec spectrum

    vector<TF1 *> responses; //!< semi-analytical integral of 2D resolution, to be integrated analytically over genpt only
    vector<TH1 *> fakeOuts, //!< fake rates for migrations outside of the phase space
                  fakeNoMatches, //!< fake rates for unmatched rec jets
                  toys; //!< pseudo rec level
    vector<vector<TH2 *>> RMs; //!< response matrices for pt and y
    vector<vector<TH1 *>> yFractions; //!< fractions used for migrations among rapidity bins // TODO: smooth?

    // TODO: make a vector of TH3 matrices ready for `getPurityStability` and `TUnfold` package

    Toy (const Scenario& s) :
        scenario(s),
        d(output->mkdir(scenario.name)),
        hGens        (nYbins, nullptr),
        hRecs        (nYbins, nullptr),
        responses    (nYbins, nullptr),
        fakeOuts     (nYbins, nullptr),
        fakeNoMatches(nYbins, nullptr),
        toys         (nYbins, nullptr),
        RMs(nYbins, vector<TH2*>(nYbins, nullptr)),
        yFractions(nYbins, vector<TH1*>(nYbins, nullptr))
    {
        cout << "\e[1m" << scenario.name << "\e[0m" << endl;

        d->cd();
        cout << "Setting up Response" << endl;
        for (int igeny = 1; igeny <= nYbins; ++igeny) {

            hGens[igeny-1] = Get<TH1>("%s/ybin%d/hGen", scenario.JERvariation, igeny);
            hGens[igeny-1]->SetName(Form("gen_ybin%d", igeny));
            hGens[igeny-1]->SetDirectory(d);

            // the histograms have one parameter per bin and contain the range in the under- and overflows
            TH1 * p_gen         = Get<TH1>("%s/ybin%d/p_fGen"      , scenario.JERvariation, igeny),
                * p_missOut     = Get<TH1>("%s/missOut/ybin%d/p_f" , scenario.JERvariation, igeny),
                * p_missNoMatch = Get<TH1>("missNoMatch/ybin%d/p_f",                        igeny),
                * p_resolution  = Get<TH1>("%s/ybin%d/p_intf2"     , scenario.JERvariation, igeny);

            // a few key numbers to find our way among the parameters
            int ngen        (p_gen        ->GetNbinsX()),
                nmissOut    (p_missOut    ->GetNbinsX()),
                nmissNoMatch(p_missNoMatch->GetNbinsX()), 
                nresolution (p_resolution ->GetNbinsX()),
                igen        (2                        ),
                imissOut    (igen        +ngen        ),
                imissNoMatch(imissOut    +nmissOut    ), 
                iresolution (imissNoMatch+nmissNoMatch),
                N           (iresolution +nresolution );

            // building functors to describe the various effects of the detector
            gens         .push_back(Thunberg<nN>   (p_gen        ->GetBinContent(0), p_gen        ->GetBinContent(1+ngen        )));
            missOuts     .push_back(Thunberg<nMiss>(p_missOut    ->GetBinContent(0), p_missOut    ->GetBinContent(1+nmissOut    )));
            missNoMatches.push_back(Thunberg<nMiss>(p_missNoMatch->GetBinContent(0), p_missNoMatch->GetBinContent(1+nmissNoMatch)));
            resolutions  .push_back(ResolutionInt2D(p_resolution ->GetBinContent(0), p_resolution ->GetBinContent(1+nresolution )));

            // lambda function used to build up the response matrix
            auto response = [this,igeny,igen,imissOut,imissNoMatch,iresolution,N](double * x, double * p) -> double {
                double genpt = x[0], recptUp = p[0], recptDn = p[1];
                double deltaUp = (recptUp-genpt)/genpt,
                       deltaDn = (recptDn-genpt)/genpt;
                if (abs(deltaUp) > w || abs(deltaDn) > w) return 0.;

                double yUp[] = {genpt, deltaUp},
                       yDn[] = {genpt, deltaDn};
                double CBup = resolutions[igeny-1](yUp, &p[iresolution]),
                       CBdn = resolutions[igeny-1](yDn, &p[iresolution]);
                if (CBup < CBdn) return 0.;

                double fGen         = gens         [igeny-1](&genpt, &p[igen        ]),
                       fMissOut     = missOuts     [igeny-1](&genpt, &p[imissOut    ])*scenario.effScale,
                       fMissNoMatch = missNoMatches[igeny-1](&genpt, &p[imissNoMatch])*scenario.effScale;

                double prefactor = 1-fMissOut-fMissNoMatch;
                prefactor = min(1.,max(0., prefactor)); // TODO ?
                return prefactor*fGen*(CBup-CBdn);
            };

            // parameters
            vector<double> p(2,0); // pt rec boundaries
            p.reserve(N);
            for (TH1 * h: {p_gen, p_missOut, p_missNoMatch, p_resolution}) {
                for (int i = 1; i <= h->GetNbinsX(); ++i) {
                    double content = h->GetBinContent(i);
                    p.push_back(content);
                    // TODO: error?
                }
            }
            assert(N == p.size());

            // rescale tail parameters (if applicable)
            for (int i = resolutions[igeny-1].ikL; i < resolutions[igeny-1].inL; ++i) p[iresolution + i] *= scenario.LHSk;
            for (int i = resolutions[igeny-1].inL; i < resolutions[igeny-1].ikR; ++i) p[iresolution + i] *= scenario.LHSn;
            for (int i = resolutions[igeny-1].ikR; i < resolutions[igeny-1].inR; ++i) p[iresolution + i] *= scenario.RHSk;
            for (int i = resolutions[igeny-1].inR; i < resolutions[igeny-1].N  ; ++i) p[iresolution + i] *= scenario.RHSn;

            // creating TF1 for response
            double ptmin = p_resolution->GetBinContent(0), ptmax = p_resolution->GetBinContent(nresolution+1);
            responses[igeny-1] = new TF1(Form("response_ybin%d", igeny), response, ptmin, ptmax, N);
            responses[igeny-1]->SetParameters(p.data());
        }

        cout << "Setting up rec-level distributions" << endl;
        for (int irecy = 1; irecy <= nYbins; ++irecy) {
            //hRecs[irecy-1] = Get<TH1>("%s/ybin%d/hRec", scenario.JERvariation, irecy);
            hRecs[irecy-1] = Get<TH1>("%s/ybin%d/hRec", "nominal", irecy);
            hRecs[irecy-1]->SetName(Form("rec_ybin%d", irecy));
            hRecs[irecy-1]->SetDirectory(d);
            toys[irecy-1] = new TH1D(Form("toy_ybin%d", irecy), "", nRecBins, recBins.data());
            toys[irecy-1]->SetDirectory(d);
            fakeOuts[irecy-1] = Get<TH1>("%s/fakeOut/ybin%d/h", scenario.JERvariation, irecy);
            fakeOuts[irecy-1]->SetName(Form("fakeOut_ybin%d", irecy));
            fakeNoMatches[irecy-1] = Get<TH1>("%s/fakeNoMatch/ybin%d/h", scenario.JERvariation, irecy);
            fakeNoMatches[irecy-1]->SetName(Form("fakeNoMatch_ybin%d", irecy));
        }

        cout << "Setting up RMs" << endl;
        for (int igeny = 1; igeny <= nYbins; ++igeny) 
        for (int irecy = max(igeny-1,1); irecy <= min(igeny+1,nYbins); ++irecy) {
            RMs[igeny-1][irecy-1] = new TH2D(Form("RM_ygenbin%d_yrecbin%d", igeny, irecy), "", nGenBins, genBins.data(), nRecBins, recBins.data());
            RMs[igeny-1][irecy-1]->SetDirectory(d);
        }

        cout << "Setting up rapidity migrations" << endl;
        TH3 * RMy = Get<TH3>("%s/RMy", scenario.JERvariation);
        for (int igeny = 1; igeny <= nYbins; ++igeny)
        for (int irecy = max(igeny-1,1); irecy <= min(igeny+1,nYbins); ++irecy) {
            TH1 * h =    RMy->ProjectionX(Form("yFrac_ygenbin%d_yrecbin%d", igeny, irecy), igeny, igeny, irecy,  irecy);
            h->Divide(h, RMy->ProjectionX(Form("yDen_ygenbin%d_yrecbin%d" , igeny, irecy), igeny, igeny,     1, nYbins), 1, 1, "b");
            yFractions[igeny-1][irecy-1] = h;
        }

        cout << "End of construction" << endl;
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Integrate
    ///
    /// Perform toy calculation
    void Integrate ()
    {
        d->cd();
        cout << "Toy calculation is starting" << endl;
        for (int igeny = 1; igeny <= nYbins; ++igeny)
        for (int irecy = max(igeny-1,1); irecy <= min(igeny+1,nYbins); ++irecy) {

            TF1 * response = responses[igeny-1];
            TH2 * RM = RMs[igeny-1][irecy-1];
            TH1 * toy = toys[igeny-1],
                * yFrac = yFractions[igeny-1][irecy-1],
                * fakeOut = fakeOuts[irecy-1],
                * fakeNoMatch = fakeNoMatches[irecy-1];

            double ptmin, ptmax;
            response->GetRange(ptmin, ptmax);

            int firstrecpt = RM->GetYaxis()->FindBin(ptmin+deps),
                lastrecpt  = RM->GetYaxis()->FindBin(ptmax-deps);
            int firstgenpt = RM->GetXaxis()->FindBin(ptmin+deps),
                lastgenpt  = RM->GetXaxis()->FindBin(ptmax-deps);
            for (int irecpt = firstrecpt; irecpt <= lastrecpt; ++irecpt)
            for (int igenpt = firstgenpt; igenpt <= lastgenpt; ++igenpt) {
                double mr = RM->GetYaxis()->GetBinLowEdge(irecpt);
                double Mr = RM->GetYaxis()->GetBinUpEdge(irecpt);
                double mg = RM->GetXaxis()->GetBinLowEdge(igenpt);
                double Mg = RM->GetXaxis()->GetBinUpEdge(igenpt);

                double content =     toy->GetBinContent(irecpt)   ,
                       error2  = pow(toy->GetBinError  (irecpt),2);

                double err = 0;
                response->SetParameter(0,Mr); response->SetParameter(1,mr);
                double integral = response->IntegralOneDim(mg, Mg, feps, feps, err);
                // TODO: could this integral be done only once instead of two or three times?

                if (integral < deps) continue;
                //cout << setw(3) << igeny << setw(3) << irecy << setw(5) << irecpt << setw(5) << igenpt << setw(15) << integral << endl;

                double w = yFrac->GetBinContent(igenpt);
                integral *= w;

                double fake = 1.+fakeOut    ->GetBinContent(irecpt)*scenario.backScale
                                +fakeNoMatch->GetBinContent(irecpt)*scenario.backScale;

                content += fake * integral;
                error2  += pow(integral*err,2);

                RM->SetBinContent(igenpt, irecpt, integral    );
                RM->SetBinError  (igenpt, irecpt, integral*err);

                toy->SetBinContent(irecpt,      content );
                toy->SetBinError  (irecpt, sqrt(error2 ));
            }
        }
        cout << "Toy calculation has finished" << endl;
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Fit
    ///
    /// Fit parameter of smooth gen spectrum to (data) rec spectrum
    void Fit (vector<TH1*> rec)
    {
        // TODO: release parameters for gen
        // TODO: release parameters for miss/fake
    }

    void Write ()
    {
        d->cd();
        for (const vector<TH1 *>& hs: {toys, hGens, hRecs}) for (TH1 * h: hs) h->Write();
        for (const vector<TH2 *>& vRM: RMs) for (TH2 * RM: vRM) if (RM != nullptr) RM->Write();
    }
};
TDirectory * Toy::input  = nullptr;
TDirectory * Toy::output = nullptr;

////////////////////////////////////////////////////////////////////////////////
/// Build toy RM including migrations among rapidity bins
void getToyRM 
             (TString input,  //!< name of input root file obtained after `fitJERcurves`
              TString output) //!< name of output root file with RMs
{
    auto source = TFile::Open(input , "READ");
    Toy::input = source;
    auto file = TFile::Open(output, "RECREATE");

    cout << "Declaring scenarios" << endl;
    vector<Scenario> pureGaussian {
              /* directory    JER     miss fake  LHSk  LHSn  RHSk  RHSn */
               {"nominal" , "nominal", 1  , 1  ,  1e8  , 1   ,  1e8  , 1   },
               {"JER_up"  , "upper"  , 1  , 1  ,  1e8  , 1   ,  1e8  , 1   },
               {"JER_dn"  , "lower"  , 1  , 1  ,  1e8  , 1   ,  1e8  , 1   },
               {"miss_up" , "nominal", 1.5, 1  ,  1e8  , 1   ,  1e8  , 1   },
               {"miss_dn" , "nominal", 0.5, 1  ,  1e8  , 1   ,  1e8  , 1   },
               {"fake_up" , "nominal", 1  , 1.5,  1e8  , 1   ,  1e8  , 1   },
               {"fake_dn" , "nominal", 1  , 0.5,  1e8  , 1   ,  1e8  , 1   },
               //{"kLHS_up"  , "nominal", 1  , 1  ,  1.1, 1   ,  1  , 1   },
               //{"kLHS_dn"  , "nominal", 1  , 1  ,  0.9, 1   ,  1  , 1   },
               //{"nLHS_up"  , "nominal", 1  , 1  ,  1  , 2   ,  1  , 1   },
               //{"nLHS_dn"  , "nominal", 1  , 1  ,  1  , 0.5 ,  1  , 1   },
               //{"nRHS_up"  , "nominal", 1  , 1  ,  1  , 1   ,  1.1, 1   },
               //{"nRHS_dn"  , "nominal", 1  , 1  ,  1  , 1   ,  0.9, 1   },
               //{"kRHS_up"  , "nominal", 1  , 1  ,  1  , 1   ,  1  , 2   },
               //{"kRHS_dn"  , "nominal", 1  , 1  ,  1  , 1   ,  1  , 0.5 },
               // TODO: include variations of rapidity migrations?
               // TODO: vary hyper parameters?
               // TODO: split among miss/fake?
    };

    cout << "Looping over scenarios for pure Gaussian" << endl;
    Toy::output = file->mkdir("pureGaussian");
    for (Scenario& scenario: pureGaussian) {
        Toy toy(scenario);
        toy.Integrate();
        toy.Write();
    }

    file->cd();

    vector<Scenario> withTails {
              /* directory    JER     miss fake  LHSk  LHSn  RHSk  RHSn */
               {"nominal" , "nominal", 1  , 1  ,  1  , 1   ,  1  , 1   },
               //{"JER_up"  , "upper"  , 1  , 1  ,  1  , 1   ,  1  , 1   },
               //{"JER_dn"  , "lower"  , 1  , 1  ,  1  , 1   ,  1  , 1   },
               //{"miss_up" , "nominal", 1.5, 1  ,  1  , 1   ,  1  , 1   },
               //{"miss_dn" , "nominal", 0.5, 1  ,  1  , 1   ,  1  , 1   },
               //{"fake_up" , "nominal", 1  , 1.5,  1  , 1   ,  1  , 1   },
               //{"fake_dn" , "nominal", 1  , 0.5,  1  , 1   ,  1  , 1   },
               {"kLHS_up"  , "nominal", 1  , 1  ,  1.1, 1   ,  1  , 1   },
               {"kLHS_dn"  , "nominal", 1  , 1  ,  0.9, 1   ,  1  , 1   },
               {"nLHS_up"  , "nominal", 1  , 1  ,  1  , 2   ,  1  , 1   },
               {"nLHS_dn"  , "nominal", 1  , 1  ,  1  , 0.5 ,  1  , 1   },
               {"nRHS_up"  , "nominal", 1  , 1  ,  1  , 1   ,  1.1, 1   },
               {"nRHS_dn"  , "nominal", 1  , 1  ,  1  , 1   ,  0.9, 1   },
               {"kRHS_up"  , "nominal", 1  , 1  ,  1  , 1   ,  1  , 2   },
               {"kRHS_dn"  , "nominal", 1  , 1  ,  1  , 1   ,  1  , 0.5 },
               // TODO: include variations of rapidity migrations?
               // TODO: vary hyper parameters?
               // TODO: split among miss/fake?
    };

    cout << "Looping over scenarios for case with tails" << endl;
    Toy::output = file->mkdir("withTails");
    for (Scenario& scenario: withTails) {
        Toy toy(scenario);
        toy.Integrate();
        toy.Write();
    }

    // TODO: fit data

    cout << "Closing input" << endl;
    source->cd();
    source->Close();
    cout << "Closing output" << endl;
    file->cd();
    file->Close();
    cout << "Done" << endl;
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    assert(__cplusplus > 201400);
    TH1::SetDefaultSumw2();
    gROOT->SetBatch();

    if (argc < 3) {
        cout << argv[0] << " input output variation\n"
             << "\twhere\tinput = output of `fitJERcurves`\n"
             << "\t     \toutput = contains RMs and toy spectra\n"
             << "NB: currently, the tails are neglected in the calculation!!\n"
             << flush;
        return EXIT_SUCCESS;
    }

    TString input = argv[1],
            output = argv[2];

    getToyRM(input, output);
    return EXIT_SUCCESS;
}
#endif
