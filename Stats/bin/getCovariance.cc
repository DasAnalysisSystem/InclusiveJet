#include <cstdlib>
#include <cassert>
#include <iostream>

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Event.h"

//#include "Core/CommonTools/interface/toolbox.h"
#include "Core/CommonTools/interface/Looper.h"
#include "Core/CommonTools/interface/MetaInfo.h"

#include <TROOT.h>
#include <TString.h>
#include <TChain.h>
#include <TFile.h>
#include <TTreeReader.h>
#include <TH1D.h>
#include <TH2D.h>

#include "Math/VectorUtil.h"

#include "common.h"

using namespace std;

////////////////////////////////////////////////////////////////////////////////
/// Template for function
void getCovariance 
             (TString input,  //!< name of input root file 
              TString output, //!< name of output root file
              int nSplit = 1, //!< number of jobs/tasks/cores
              int nNow = 0)   //!< index of job/task/core
{
    /// Opening source and checking that it is MC
    TFile * source = TFile::Open(input, "READ");
    TTree * tree = dynamic_cast<TTree *>(source->Get("inclusive_jets"));

    MetaInfo metainfo(tree);
    if (nNow == 0) metainfo.Print();

    TTreeReader reader(tree);
    TTreeReaderValue<Event> ev = {reader, "event"};
    TTreeReaderValue<vector<RecJet>> jets = {reader, "recJets"};
    TTreeReaderValue<PrimaryVertex> vtx = {reader, "primaryvertex"};

    TFile * file = TFile::Open(output, "RECREATE");

    int n = nYbins*nRecBins;
    TH1 * h   = new TH1F("h"  , "h"  , n, 0.5, n+0.5);
    TH2 * cov = new TH2F("cov", "cov", n, 0.5, n+0.5,
                                       n, 0.5, n+0.5);

    const double minpt = 74; // TODO: check that this is consistent with cut in `{get,fit}JERcurves` 

    Looper looper(__func__, &reader, nSplit, nNow);
    while (looper.Next()) {

        if (abs(vtx->z) > 24 /* cm */) continue;

        double evWgt = ev->weights.front();
        for (const auto& jet: *jets) {

            double jetWgt = jet.weights.front();
            double w = jetWgt*evWgt;

            double pt = jet.CorrPt(),
                   absy = abs(jet.Rapidity());

            if (pt < minpt) continue;
           
            int ibin = getbin(pt, absy);
            h->Fill(ibin, w);

            for (const auto& jet2: *jets) {
                double jetWgt2 = jet2.weights.front();
                double w2 = jetWgt2*evWgt;

                double pt2 = jet2.CorrPt(),
                       absy2 = abs(jet2.Rapidity());

                if (pt2 < minpt) continue;
           
                int ibin2 = getbin(pt2, absy2);
                cov->Fill(ibin, ibin2, w*w2);
            }
        }
    }

    //if (nNow == 0) {
    //    cout << setw(5) << "iy" << setw(5) << "ipt" << setw(5) << "y" << setw(10) << "pt" << setw(5) << "index\n";
    //    for (size_t iy = 1; iy <= nYbins; ++iy)
    //    for (size_t ipt = 1; ipt <= nPtBins; ++ipt) {
    //        int index = getbin(ipt,iy);
    //        double xsect = h->GetBinContent(index),
    //               unc = sqrt(cov->GetBinContent(index,index));
    //        cout << setw(5) << iy << setw(5) << ipt << setw(5) << y << setw(10) << pt << setw(5) << index << setw(20) << xsect << setw(20) << unc << '\n';
    //    }
    //    cout << flush;
    //}

    file->Write();
    file->Close();
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    TH1::SetDefaultSumw2();
    gROOT->SetBatch();

    if (argc < 3) {
        cout << argv[0] << " input output [nSplit [nNow]]" << endl;
        return EXIT_SUCCESS;
    }

    TString input = argv[1],
            output = argv[2];
    int nNow = 0, nSplit = 1;
    if (argc > 3) nSplit = atoi(argv[3]);
    if (argc > 4) nNow = atoi(argv[4]);

    getCovariance(input, output, nSplit, nNow);
    return EXIT_SUCCESS;
}
#endif
