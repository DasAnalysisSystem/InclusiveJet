# Unfolding

Several executables exist to get the histograms, depending on the variable to unfold.
But there is only one executable to perform the unfolding.

## Before the funolding: `getUnfHistograms*`

Use `getUnfHistogramsPt` to get histograms as a function of the transverse momentum only.
Here, there is no need to use `TUnfoldBinning`, and native ROOT histograms are used.

Use `getUnfHistogramsPtY` to get histograms as a function of the transverse momenum and of the rapidity.
Here, we use `TUnfoldBinning` to map 2D distributions on 1D ROOT histograms.
However, the exact same structure is used for the output ROOT file as for `getUnfHistogramsPt`.

For the RΔφ analysis, use `getUnfHistogramsPtNnbr` to get histograms as a function of the transverse momentum and number of neighbours.
The implementation is analog to that of `getUnfHistogramsPtY`, with some differences regarding the background and inefficiencies (fake and miss entries).
The structure of the output is always the same.

## Unfolding: `unfold`

A single executable to unfold all the distribution in the same way with `TUnfold`.
It works with 1D histograms, regardless of whether they correspond to mappings with `TUnfoldBinning` or to plain ROOT 1D histograms.

## After the unfolding

### Inclusive jet analysis: `printTables`

Produces:
 - xFitter tables
 - LaTeX tables (either for beamer or for plain text)
 - covariance/correlation tables

### RΔφ analysis: `getRdphiRatio`

Uses a toy calculation (see example in `$CMSSW_BASE/src/Core/CommonTools/test/toy.cc`) to estimate the RΔφ observable from the 2D distributions.
The structure of the output file is exactly the same as the input file, i.e. it is the same structure as if it were the output of `unfold`.
