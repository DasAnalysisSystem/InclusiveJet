#include <TH2.h>
#include <TString.h>
#include <TDirectory.h>

////////////////////////////////////////////////////////////////////////////////
/// Save correlation matrix
void SaveCorr (TH2 * cov, TString name, TString title, TDirectory * d)
{
    TH2 * corr = dynamic_cast<TH2*>(cov->Clone(name));
    corr->Reset();
    const int Nx = corr->GetNbinsX(),
              Ny = corr->GetNbinsY();

    for (int x = 1; x <= Nx; ++x) {
        double denx = sqrt(cov->GetBinContent(x, x));
        for (int y = 1; y <= Ny; ++y) {
            double deny = sqrt(cov->GetBinContent(y, y));

            if (denx > 0 && deny > 0) {
                double content = cov->GetBinContent(x, y);
                content /= denx*deny;
                corr->SetBinContent(x, y, content);
                if (x != y && abs(content) >= 1.) cout << x << ' ' << y << ' ' << content << endl;
            }
            else 
                corr->SetBinContent(x, y, 0);
        }
    }
    corr->SetDirectory(d);
    d->cd();
    corr->Write(name);
}
