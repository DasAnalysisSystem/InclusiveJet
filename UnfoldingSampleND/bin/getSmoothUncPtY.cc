#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>
#include <numeric>
#include <limits>

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Event.h"

#include "Core/CommonTools/interface/toyEigen.h"
#include "Core/CommonTools/interface/toyRoot.h"
#include "Core/CommonTools/interface/Greta.h"

#include <TROOT.h>
#include <TString.h>
#include <TChain.h>
#include <TFile.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TProfile.h>
#include <TList.h>
#include <TKey.h>
#include <TDecompSVD.h>

#include "Math/VectorUtil.h"
#include "Math/Minimizer.h"
#include "Math/Factory.h"
#include "Math/Functor.h"

#include "PtY.h"
#include "getsystematics.h"

using namespace std;
using namespace DAS;
using namespace InclusiveJet;

////////////////////////////////////////////////////////////////////////////////
/// 2D fit function based on Chebyshev polynomials TODO
//struct GretaLogPtY : public Greta {
//
//    const int degree2, npars2;
//    const double m2, M2;
//
//    Greta2D (int d, int d2, double mi, double Ma, double mi2, double Ma2) :
//        Greta(d,mi,Ma),
//        degree2(d2), npars2(d2+1), m2(mi2), M2(Ma2)
//    { }
//
//    inline int index (int i, int j) { return this->npars*j + i; }
//
//    double operator() (double *x, const double *p)
//    {
//        // normalise the variables into [-1,1]
//        double nx = -1 + 2 * (log(x[0]) - log(m)) / (log(M) - log(m));
//        double ny = -1 + 2 * (    x[1]  -     m2) / (    M2 -     m2);
//
//        double result = 0;
//        for (int i = 0; i <= degree ; ++i)   // runs over pt axis
//        for (int j = 0; j <= degree2; ++j) { // runs over y  axis
//            int I = index(i,j);
//            assert(I < npars*npars2);
//            result += p[I] * T(nx,i) * T(ny,j);
//        }
//        result = exp(result);
//        return result;
//    }
//};

TH2 * GetCovY (TH2 * cov2, int y)
{
    auto cov1 = new TH2D(Form("cov_%d", y), "", nGenBins, genBins.data(), nGenBins, genBins.data());
    int iminpt = cov1->GetXaxis()->FindBin(minpt),
        imaxpt = cov1->GetXaxis()->FindBin(maxpt);
    for (int ipt1 = iminpt; ipt1 <= imaxpt; ++ipt1) 
    for (int ipt2 = iminpt; ipt2 <= imaxpt; ++ipt2) {
        auto pt1 = cov1->GetXaxis()->GetBinCenter(ipt1),
             pt2 = cov1->GetYaxis()->GetBinCenter(ipt2);
        auto ibin1 = genBinning->GetGlobalBinNumber(pt1,0.5*(y_edges.at(y)+y_edges.at(y-1))),
             ibin2 = genBinning->GetGlobalBinNumber(pt2,0.5*(y_edges.at(y)+y_edges.at(y-1)));
        auto content = cov2->GetBinContent(ibin1, ibin2);
        cov1->SetBinContent(ipt1, ipt2, content);
    }
    return cov1;
}

template<int d> struct Correlator {

    const int n;
    TVectorD c;
    TVectorD v; //!< xsection value
    TMatrixD cov; //!< covariance matrix 

    Thunberg<d, log, exp>& greta;

    Correlator (TH1 * h, TH2 * hcov, int im, int iM, Thunberg<d,log,exp>& g) :
        n(iM - im + 1), c(n), v(n), cov(n,n), greta(g)
    {
        cout << "n = " << n << endl;

        //h->Print("all");
        cout << "Converting from histograms to vector and matrix:";
        int shift = im;
        for (int i = 0; i < n; ++i) {
            c(i) = h->GetBinCenter (i+shift);
            v(i) = h->GetBinContent(i+shift);
            cout << '\n' << setw(5) << i << setw(10) << c(i) << setw(15) << v(i) << setw(15);
            assert(abs(v(i)) > 0);
            for (int j = 0; j < n; ++j) {
                cov(i,j) = hcov->GetBinContent(i+shift,j+shift);
                if (i == j) cout << cov(i,j);
            }
        }
        cout << endl;

        cout << "Getting eigenvalues";
        TDecompSVD svd(cov);
        TVectorD v = svd.GetSig();
        for(int k = 0; k < n; ++k) {
            //cout << '\n' << setw(5) << k << setw(15) << v[n-k-1] << setw(15) << cov(k,k);
            assert(v[k] > 0);
        }
        cout << endl;

        cout << "Inverting:" << endl;
        bool status = false;
        cov = svd.Invert(status);
        assert(status);
        cout << "Inverted." << endl;
    }

    double operator() (const double * p)
    {
        auto r = v;

        //for (int i = 0; i <= greta.npars; ++i) cout << p[i] << ' ';
        //cout << endl;

        // using integral
        for (int i = 0; i < n; ++i) {
            double pt = c(i);
            double prediction = greta(&pt, p);
            r(i) -= prediction;
            //cout << setw(10) << pt << setw(15) << prediction << setw(15) << v(i) << setw(15) << r(i) << '\n';
        }
        //cout << endl;

        auto result = r * (cov * r);
        //assert(isnormal(result));
        return result;
    }
};

template<int maxdegree, bool autoStop = true>
TF1 * GetSmoothFit (TH1 * h, TH2 * cov, double m, double M, double nSigmaStop)
{
    cout << m << ' ' << M << endl;
    assert(m < M);

    int im = h->FindBin(m);
    int iM = h->FindBin(M-0.001);
    cout << "im = " << im << ", iM = " << iM << endl;
    assert(im < iM);

    double fm = h->GetBinContent(im),
           fM = abs(h->GetBinContent(iM));
    cout << fm << ' ' << fM << endl;
    fm = log(fm),
    fM = log(fM);
    cout << fm << ' ' << fM << endl;
    assert(isnormal(fm) && isnormal(fM));

    Thunberg<maxdegree, log, exp> greta(m, M);
    Correlator<maxdegree> correlator(h, cov, im, iM, greta);
    ROOT::Math::Functor functor(correlator, greta.npars);

    ROOT::Math::Minimizer* minimiser = ROOT::Math::Factory::CreateMinimizer("Minuit2", "Migrad");
    minimiser->SetMaxFunctionCalls(1e6);
    minimiser->SetFunction(functor);
    minimiser->SetTolerance(0.001);
    minimiser->SetPrintLevel(0);

    double step = 0.001;
    minimiser->SetVariable(0,"p0",(fM+fm)/2,step);
    minimiser->SetVariable(1,"p1",(fM-fm)/2,step);
    for (int degree = 2; degree <= maxdegree; ++degree) {
        minimiser->SetVariable(degree,Form("p%d", degree),0.,step);
        minimiser->FixVariable(degree);
    }

    auto dump = [minimiser](int n) {
        const double * X = minimiser->X();
        cout << "parameters (degree = " << (n-1) << "): ";
        for (int i = 0; i < n; ++i) cout << setw(15) << X[i];
        cout << endl;
    };

    auto X = minimiser->X();
    double lastchi2 = correlator(X);
    cout << "before fitting: chi2 = " << lastchi2 << endl;

    dump(2);

    //running
    for (int degree = 1; degree <= maxdegree; ++degree) {

        minimiser->ReleaseVariable(degree);
        /*bool state =*/ minimiser->Minimize();

        int status = minimiser->Status();
        if (status > 0) switch (status) {
            case 1:  cerr << "Warning: Covariance was made positive defined\n"; break;
            case 2:  cerr << "Warning: Hesse is invalid\n"; break;
            case 3:  cerr << "Warning: Expected Distance reached from the Minimum is above max (EDM = " << minimiser->Edm() << ")\n"; break;
            case 4:  cerr << "Warning: Reached call limit\n"; break;
            case 5:
            default: cerr << "Warning: Any other failure\n";
        }

        const double * X = minimiser->X();
        double chi2 = correlator(X);
        assert(isnormal(chi2));
        int ndf = iM - im - degree;

        double x2n = chi2/ndf,
               error = sqrt(2./ndf);

        cout << "\e[1m" << degree << ' ' << status << '\t' << chi2 << ' ' << ndf << '\t' << x2n << "\u00B1" << error << "\e[0m" << endl;
        dump(degree);

        if (autoStop && x2n > lastchi2/(ndf+1)) {
            cout << "Chi2/ndf has increased -- stepping back and stopping!" << endl;
            minimiser->SetVariableValue(degree,0.);
            minimiser->FixVariable(degree);
            --degree;
            minimiser->Minimize();
            break;
        }
        if (abs(x2n-1) <= nSigmaStop*error) break;
        if (ndf == 2) break;
        lastchi2 = chi2;
    }

    TF1 * f = new TF1(Form("f_%s", h->GetName()), greta, m, M, greta.npars);
    X = minimiser->X();
    f->SetParameters(X);
    return f;
}

void getSmoothUncPtY
             (TString input,  //!< name of input root file 
              TString output) //!< name of output root file
{
    InitBinning();

    TFile * source = TFile::Open(input, "READ");

    auto nom = dynamic_cast<TH1*>(source->Get("nominal"));
    auto divide = [nom](TH1 * v) {
        for (int i = 1; i <= nom->GetNbinsX(); ++i) {
            double num = v->GetBinContent(i),
                   sig = v->GetBinError  (i),
                   den = nom->GetBinContent(i);
            v->SetBinContent(i, den > 0 ? num/den : 0);
            v->SetBinError  (i, den > 0 ? sig/den : 0);
        }
        return v;
    };

    //auto cov = dynamic_cast<TH2*>(source->Get("cov"));
    auto covUncor = dynamic_cast<TH2*>(source->Get("covUncor"));

    auto relative = [nom](TH2 * cov2) {
        for (int i = 1; i <= nom->GetNbinsX(); ++i) {
            double content = nom->GetBinContent(i);
            if (abs(content) < numeric_limits<double>::epsilon()) continue;
            for (int j = 1; j <= nom->GetNbinsX(); ++j) {
                {
                    double sig2 = cov2->GetBinContent(i,j);
                    cov2->SetBinContent(i,j,sig2/content);
                }

                {
                    double sig2 = cov2->GetBinContent(j,i);
                    cov2->SetBinContent(j,i,sig2/content);
                }
            }
        }
    };

    //relative(cov);
    relative(covUncor);

    auto dJES = dynamic_cast<TDirectory *>(source->Get("JES")); // variations
    vector<TH1 *> JES = GetSystematics(dJES, "");

    auto dMC = dynamic_cast<TDirectory *>(source->Get("MC")); // shifts
    vector<TH1 *> MC = GetSystematics(dMC, "");
    auto missUp = dynamic_cast<TH1 *>(nom->Clone("missup")),
         missDn = dynamic_cast<TH1 *>(nom->Clone("missdown")),
         fakeUp = dynamic_cast<TH1 *>(nom->Clone("fakeup")),
         fakeDn = dynamic_cast<TH1 *>(nom->Clone("fakedown"));
    for (auto it = MC.begin(); it != MC.end(); /* nothing */) {
        TString name = (*it)->GetName();
        if (name.Contains("miss")) {
            if (name.Contains("up")) missUp->Add(*it);
            else /*if (name.Contains("down"))*/ missDn->Add(*it);
            MC.erase(it);
        }
        else if (name.Contains("fake")) {
            if (name.Contains("up")) fakeUp->Add(*it);
            else /*if (name.Contains("down"))*/ fakeDn->Add(*it);
            MC.erase(it);
        }
        else ++it;
    }
    MC.push_back(missUp);
    MC.push_back(missDn);
    MC.push_back(fakeUp);
    MC.push_back(fakeDn);

    auto file = TFile::Open(output, "RECREATE");
    dJES = file->mkdir("JES");
    dMC = file->mkdir("MC");

    vector<pair<TDirectory *, vector<TH1*>>> all_shifts;
    all_shifts.push_back({ dJES, JES });
    all_shifts.push_back({ dMC , MC  });

    for (auto& shifts: all_shifts)
    for (vector<TH1*>::iterator shift = shifts.second.begin(); shift != shifts.second.end(); shift += 2) {
    //for (TH1 * h: shifts.second) {

        TH1 * hUp = *shift,
            * hDn = *(next(shift));

        TDirectory * dir = shifts.first;
        dir->cd();

        TString n = hUp->GetName();
        n.ReplaceAll("up", "");

        cout << "================================\n"
             << "\e[1m" << n << "\e[0m" << endl;

        if (n == "AbsoluteStat") continue;
        if (n.Contains("HF")) continue;
        //if (n.Contains("EC2")) continue; // TODO?

        if (n == "Pref") {
            hUp->SetDirectory(dir);
            hDn->SetDirectory(dir);
            dir->cd();
            hUp->Write();
            hDn->Write();
            continue;
        }

        divide(hUp);
        divide(hDn);

        auto hUp2 = dynamic_cast<TH2*>(genBinning->ExtractHistogram(n + "2D", hUp));
        auto hDn2 = dynamic_cast<TH2*>(genBinning->ExtractHistogram(n + "2D", hDn));
        hUp2->SetDirectory(0);
        hDn2->SetDirectory(0);

        auto covUp2 = dynamic_cast<TH2*>(source->Get(n == "miss" || n == "fake" ? "cov" : "JEScov/" + n + "up"  )->Clone(n + "_covUp"));
        auto covDn2 = dynamic_cast<TH2*>(source->Get(n == "miss" || n == "fake" ? "cov" : "JEScov/" + n + "down")->Clone(n + "_covDn"));
        relative(covUp2);
        relative(covDn2);
        covUp2->Add(covUncor);
        covDn2->Add(covUncor);

        cout << __LINE__ << "\tLooping over rapidity bins" << endl;
        for (int ybin = 1; ybin <= nYbins; ++ybin) {
            cout << "---------------------------------\n"
                 << "\e[1my = " << ybin << "\e[0m" << endl;
            double y = ybin*0.5-0.25;

            // getting 1D histogram
            TString name1D = n + Form("1D_ybin%d",ybin);
            auto hUp1 = hUp2->ProjectionX(name1D + "Up", ybin, ybin);
            auto hDn1 = hDn2->ProjectionX(name1D + "Dn", ybin, ybin);
            auto covUp1 = GetCovY(covUp2, ybin);
            auto covDn1 = GetCovY(covDn2, ybin);

            hUp1->Print("all");
            hDn1->Print("all");

            double ptMin = 97;
            double nSigmaStop = 1;
            if (n == "JER" || n == "RelativeSample" || n == "RelativeFSR") nSigmaStop = 0.5; // TODO?
            vector<double> ptMax { 3450, 2787, 2500, 1588, 1248};
            //static vector<double> ptMax { 3103 /*3450*/, 2787, 2238 /*2500*/, 1588 /*1784*/, 1248 /*1500*/};
            //double nSigmaStop = n.Contains("Pref") || n.Contains("JER")
            auto fDn = GetSmoothFit<4,true>(hDn1, covDn1, ptMin, ptMax.at(ybin-1), nSigmaStop);
            if (n == "JER") ptMin = 74; // TODO?
            //if (n == "AbsoluteScale") ptMax.at(0) = 2787;
            auto fUp = GetSmoothFit<4,true>(hUp1, covUp1, ptMin, ptMax.at(ybin-1), nSigmaStop);

            double ptmin, ptmax;
            fUp->GetRange(ptmin, ptmax);
            int iptmin = hUp1->GetXaxis()->FindBin(ptmin),
                iptmax = hUp1->GetXaxis()->FindBin(ptmax);
            for (int ipt = iptmin; ipt <= iptmax; ++ipt) {
                double //ptlow    = hUp1->GetBinLowEdge(ipt),
                       ptcenter = hUp1->GetBinCenter (ipt);
                       //pthigh   = hUp1->GetBinLowEdge(ipt+1);
                int i = genBinning->GetGlobalBinNumber(ptcenter, y);
                double content = nom->GetBinContent(i);

                double varUp = fUp->Eval(ptcenter)-1;
                double varDn = fDn->Eval(ptcenter)-1;

                double sign = varUp > 0 ? 1 : -1;
                double var = max( abs(varUp), abs(varDn));

                hUp->SetBinContent(i, content*(1+sign*var));
                hDn->SetBinContent(i, content*(1-sign*var));

                //cout << setw(5) << i << setw(10) << ptlow << setw(10) << ptcenter << setw(10) << pthigh << setw(15) << oldcontent << setw(15) << newcontent << setw(15) << newcontent/oldcontent << '\n';
            }
            cout << endl;

            delete covUp1;
            delete covDn1;
            break;
        }

        cout << __LINE__ << "\tWriting output" << endl;
        hUp->SetDirectory(dir);
        hDn->SetDirectory(dir);
        dir->cd();
        hUp->Write();
        hDn->Write();
        break;
    }

    cout << __LINE__ << "\tClosing" << endl;
    file->Close();
    source->Close();
    cout << __LINE__ << "\tDone" << endl;
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    TH1::SetDefaultSumw2();
    gROOT->SetBatch();

    if (argc < 3) {
        cout << argv[0] << " input output" << endl;
        cout << "Warning: this executable has been tuned for the inclusive jet analysis with 2016 ReReco data! Use it at your own risk if you are doing a more recent analysis..." << endl;
        return EXIT_SUCCESS;
    }

    TString input = argv[1],
            output = argv[2];

    getSmoothUncPtY(input, output);
    return EXIT_SUCCESS;
}
#endif
