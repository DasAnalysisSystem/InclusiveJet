#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Event.h"

#include <cmath>
#include <vector>

#include <TTreeReader.h>

using namespace std;

namespace DAS::ModelVariations {

////////////////////////////////////////////////////////////////////////////////
/// Model reweighting as a function of the hard scale of the event
void Scale
    (Event& ev)
{
    float nominal = ev.genWgts.front();
    ev.genWgts.push_back(nominal*(     3.+ev.hard_scale/600));
    ev.genWgts.push_back(nominal*(39./18.-ev.hard_scale/600));
}

////////////////////////////////////////////////////////////////////////////////
/// Model reweighting as a function of the pt of the leading jet
void Pt
    (Event& ev,
    const vector<RecJet>& recjets,
    const vector<GenJet>& genjets)
{
    float pt0 = 50; // TODO?
    if (recjets.size() > 0)
        pt0 = recjets.front().CorrPt(0);
    else if (genjets.size() > 0)
        pt0 = genjets.front().p4.Pt();
    // this corresponds to 1% variation up at 100  GeV
    //                            and down at 1000 GeV
    // for the leading jet pt
    float dW = 0.02 * log10(pt0) - 0.05;
    float nominal = ev.genWgts.front();
    ev.genWgts.push_back(nominal*(1.+dW));
    ev.genWgts.push_back(nominal*(1.-dW));
}

////////////////////////////////////////////////////////////////////////////////
/// Model reweighting as a function of the y of the leading jet
void Y
    (Event& ev,
    const vector<RecJet>& recjets,
    const vector<GenJet>& genjets)
{
    float y0 = 0;
    if (recjets.size() > 0)
        y0 = recjets.front().Rapidity();
    else if (genjets.size() > 0)
        y0 = genjets.front().Rapidity();
    // this corresponds to 1% variation at 1.0
    // for the leading jet pt
    float dW = 0.01 * abs(y0);
    float nominal = ev.genWgts.front();
    ev.genWgts.push_back(nominal*(1.+dW));
    ev.genWgts.push_back(nominal*(1.-dW));
}

}
