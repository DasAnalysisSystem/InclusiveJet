#include "Core/CommonTools/interface/variables.h"

#include <vector>

#include <TUnfold/TUnfoldBinning.h>

namespace DAS {
namespace JetMultiplicity {

static const auto minpt = DAS::recBins.at(2) /* 74 */, maxpt = DAS::recBins.back(), maxy = DAS::y_edges.back();
//static const std::vector<float> maxpts { 3103, 2787, 2500, 1784, 1101};

static const vector<double> n_edges { 0.5, 1.5, 2.5, 3.5, 4.5 };
static const int nNbins = n_edges.size()-1;
static const auto minN = n_edges.front(), maxN = n_edges.back();

static TUnfoldBinning * recBinning = new TUnfoldBinning("recBinning");
static TUnfoldBinning * genBinning = new TUnfoldBinning("genBinning");

void InitBinning () 
{
    recBinning->AddAxis("recpt",nRecBins,recBins.data(),false,false);
    recBinning->AddAxis("recN" ,  nNbins,n_edges.data(),false,false);
    genBinning->AddAxis("genpt",nGenBins,genBins.data(),false,false);
    genBinning->AddAxis("genN" ,  nNbins,n_edges.data(),false,false);
}

}
}


