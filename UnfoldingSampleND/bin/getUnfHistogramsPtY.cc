#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>
#include <utility>

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/stream.h"
#include "Core/Objects/interface/Event.h"

#include "Core/CommonTools/interface/toolbox.h"
#include "Core/CommonTools/interface/Looper.h"
#include "Core/CommonTools/interface/MetaInfo.h"

#include "Core/JEC/interface/JESreader.h"

#include <TROOT.h>
#include <TString.h>
#include <TFile.h>
#include <TTreeReader.h>
#include <TH1D.h>
#include <TH2D.h>

#include "Math/VectorUtil.h"
#include "Math/GenVector/GenVector_exception.h"

#include "Core/JEC/bin/matching.h"

#include "model.h"
#include "InclusiveJet/ModelReweighting/bin/model.h"
#include "PtY.h"
#include "JetPairWeight.h"

using namespace std;

using namespace DAS;
using namespace DAS::InclusiveJet;

struct Variation {
    const TString name;
    static bool isMC;
    const size_t iJEC, iJetWgt, iEvtWgt;
    TH1 * rec, * tmp, * gen,
        * missNoMatchBB, * missNoMatchEC, * missOutY, * missOutLow, * missOutHigh,
        * fakeNoMatchBB, * fakeNoMatchEC, * fakeOutY, * fakeOutLow, * fakeOutHigh;    
    TH2 * cov, * RM;

    Variation (TString Name, size_t IJEC = 0 , size_t IJetWgt = 0 , size_t IEvtWgt = 0) :
        name(Name), iJEC(IJEC), iJetWgt(IJetWgt), iEvtWgt(IEvtWgt),
        rec(nullptr), tmp(nullptr), gen(nullptr),
        missNoMatchBB(nullptr), missNoMatchEC(nullptr), missOutY(nullptr), missOutLow(nullptr), missOutHigh(nullptr),
        fakeNoMatchBB(nullptr), fakeNoMatchEC(nullptr), fakeOutY(nullptr), fakeOutLow(nullptr), fakeOutHigh(nullptr),
        cov(nullptr), RM(nullptr)
    {
        rec = recBinning->CreateHistogram(Name + "rec", false, 0, "detector level"),
        tmp = recBinning->CreateHistogram(Name + "tmp");
        cov = recBinning->CreateErrorMatrixHistogram(Name + "cov", false, 0, "covariance matrix at detector level");
        if (!isMC) return;
        gen           = genBinning->CreateHistogram(Name + "gen"          , false, 0, "hadron level"),
        missNoMatchBB = genBinning->CreateHistogram(Name + "missNoMatchBB", false, 0, "real inefficiency in barrel region"),
        missNoMatchEC = genBinning->CreateHistogram(Name + "missNoMatchEC", false, 0, "real inefficiency in endcap region"),
        missOutY      = genBinning->CreateHistogram(Name + "missOutY"     , false, 0, "good gen jets matched to jets reconstructed at too high rapidity"),
        missOutHigh   = genBinning->CreateHistogram(Name + "missOutHigh"  , false, 0, "good gen jets matched to jets reconstructed at too high pt"      ),
        missOutLow    = genBinning->CreateHistogram(Name + "missOutLow"   , false, 0, "good gen jets matched to jets reconstructed at too low pt"       ),
        fakeNoMatchBB = recBinning->CreateHistogram(Name + "fakeNoMatchBB", false, 0, "real background in barrel region"),
        fakeNoMatchEC = recBinning->CreateHistogram(Name + "fakeNoMatchEC", false, 0, "real background in endcap region"),
        fakeOutY      = recBinning->CreateHistogram(Name + "fakeOutY"     , false, 0, "good rec jets matched to jets generated at too high rapidity"),
        fakeOutHigh   = recBinning->CreateHistogram(Name + "fakeOutHigh"  , false, 0, "good rec jets matched to jets generated at too high pt"      ),
        fakeOutLow    = recBinning->CreateHistogram(Name + "fakeOutLow"   , false, 0, "good rec jets matched to jets generated at too low pt"       );
        RM            = TUnfoldBinning::CreateHistogramOfMigrations(genBinning, recBinning, Name + "RM", false, false, "migrations of jets in phase space at both levels");
    }

    void Write (TFile * file)
    {
        cout << "Writing " << name << endl;
        file->cd();
        TDirectory * dir = file->mkdir(name);
        dir->cd();
#define PAIR(name) make_pair(dynamic_cast<TH1*>(name), #name)
        for (auto p: { PAIR(gen), PAIR(missNoMatchBB), PAIR(missNoMatchEC), PAIR(missOutY), PAIR(missOutHigh), PAIR(missOutLow),
                       PAIR(rec), PAIR(fakeNoMatchBB), PAIR(fakeNoMatchEC), PAIR(fakeOutY), PAIR(fakeOutHigh), PAIR(fakeOutLow),
                       PAIR(cov), PAIR(RM) }) {
#undef PAIR
            if (p.first == nullptr) continue;
            p.first->SetDirectory(dir);
            p.first->Write(p.second);
        }
    }
};
bool Variation::isMC = false; // just fort initialisation

////////////////////////////////////////////////////////////////////////////////
/// Get histograms for unfolding for both pt and y (in d=2), 
/// using `TUnfoldBinning`.
///
/// Then the output can be unfolded using `unfold`.
void getUnfHistogramsPtY 
             (TString input,  //!< name of input root file 
              TString output, //!< name of output root file
              TString model,  //!< name of root file containing smooth reweighting with multiplicity
              int nSplit = 1, //!< number of jobs/tasks/cores
              int nNow = 0)   //!< index of job/task/core
{
    InitBinning();

    TFile * source = TFile::Open(input, "READ");
    TTree * tree = dynamic_cast<TTree *>(source->Get("inclusive_jets"));

    cout << "Setting up branches" << endl;

    MetaInfo metainfo(tree);
    if (nNow == 0) metainfo.Print();
    bool isMC = metainfo.isMC();

    Event * ev = nullptr;
    tree->SetBranchAddress("event", &ev);
    vector<RecJet> * recJets = nullptr;
    vector<GenJet> * genJets = nullptr;
    tree->SetBranchAddress("recJets", &recJets);
    if (isMC) 
        tree->SetBranchAddress("genJets", &genJets);

    TFile * file = TFile::Open(output, "RECREATE");

    cout << "Setting variations" << endl;

    Variation::isMC = isMC;
    vector<Variation> variations { Variation("nominal") };
    if (isMC) {
        variations.push_back( Variation("JERup"      , 1, 0, 0) );
        variations.push_back( Variation("JERdown"    , 2, 0, 0) );
        variations.push_back( Variation("PUup"       , 0, 0, 1) );
        variations.push_back( Variation("PUdown"     , 0, 0, 2) );
    }
    else {
        variations.push_back( Variation("Prefup"     , 0, 0, 1) );
        variations.push_back( Variation("Prefdown"   , 0, 0, 2) );
        int i = 1;
        for (TString& JES: JESreader::getJECUncNames()) {
            variations.push_back( Variation(JES + "up"  , i  , 0, 0) );
            variations.push_back( Variation(JES + "down", i+1, 0, 0) );
            i += 2;
        }
    }

    vector<JetPairWeight> jpWgts;

    cout << "Looping" << endl;
    Looper looper(__func__, tree, nSplit, nNow);
    while (looper.Next()) {
        jpWgts.clear();

        //size_t NRec =        recJets->size(),
        //       NGen = isMC ? genJets->size() : 0;

        size_t NRec =        count_if(recJets->begin(), recJets->end(), [](RecJet& recJet) { return recJet.CorrPt() > 74;}),
               NGen = isMC ? count_if(genJets->begin(), genJets->end(), [](GenJet& genJet) { return genJet.p4.Pt() > 74;}) : 0;
        // MODEL REWEIGHTING

        if (isMC) {

            assert(ev->weights.size() == 3);

            auto get_iRec = [recJets](const RecJet& recJet) {
                auto thispt = recJet.CorrPt(0);
                for (size_t iRec = 0; iRec < recJets->size(); ++iRec) {
                    auto thatpt = recJets->at(iRec).CorrPt(0);
                    if (abs(thispt - thatpt) < feps)
                        return iRec;
                }
                cerr << "Rec jet was not found!\n";
                exit(EXIT_FAILURE);
            };

            static Model m(model, nNow == 0);
            Matching<RecJet, GenJet> matching(*recJets);
            for (size_t iGen = 0; iGen < NGen; ++iGen) {
                const GenJet& genJet = genJets->at(iGen);
                auto gpt = genJet.p4.Pt(),
                     gy  = abs(genJet.Rapidity());

                const RecJet& recJet = matching.HighestPt(genJet);
                bool noMatch = recJet.p4.Pt() > 13e3 /* 13 TeV */;
                if (noMatch) {
                    auto nGen = iGen + 1;
                    auto jWgt = m(gpt, gy, nGen, NGen);
                    
                    jpWgts.push_back( { iGen, NRec, jWgt } );
                }
                else {
                    auto rpt = recJet.CorrPt(0),
                         ry  = abs(recJet.Rapidity());
                    auto iRec = get_iRec(recJet),
                         nRec = iRec + 1;
                    auto jWgt = m(rpt, ry, nRec, NRec);

                    jpWgts.push_back( { iGen, iRec, jWgt } );
                }
            }

            for (const RecJet& recJet: matching.mcands) {
                auto rpt = recJet.CorrPt(0),
                     ry  = abs(recJet.Rapidity());
                auto iRec = get_iRec(recJet),
                     nRec = iRec + 1;
                auto jWgt = m(rpt, ry, nRec, NRec);

                jpWgts.push_back( { NGen, iRec, jWgt } );
            }
        }
        else
            for (size_t iRec = 0; iRec < NRec; ++iRec)
                jpWgts.push_back( { 1, iRec, 1.} );

        // FILLING HISTOGRAMS FOR UNFOLDING

        for (Variation& v: variations) {

            v.tmp->Reset();

            auto evWgt = ev->weights.at(v.iEvtWgt);

            vector<int> binIDs; // we save the indices of the filled bins in order to avoid many multiplications by 0
            for (auto& jpWgt: jpWgts) {

                if (jpWgt.iRec >= NRec) continue;
                RecJet& recJet = recJets->at(jpWgt.iRec);

                auto y  = recJet.AbsRap();
                if (y >= maxy) continue;
                auto pt = recJet.CorrPt(v.iJEC);
                if (pt < minpt || pt > maxpt) continue;

                auto irecbin = recBinning->GetGlobalBinNumber(pt,y);
                if (find(binIDs.begin(), binIDs.end(), irecbin) == binIDs.end()) binIDs.push_back(irecbin);

                auto jWgt = recJet.weights.at(v.iJetWgt);

                v.rec->Fill(irecbin, evWgt * jpWgt.weight * jWgt);
                v.tmp->Fill(irecbin, evWgt * jpWgt.weight * jWgt); // for cov
            }

            for (auto x: binIDs) for (auto y: binIDs) {
                auto cCov = v.cov->GetBinContent(x,y),
                     cTmp = v.tmp->GetBinContent(x)*v.tmp->GetBinContent(y);
                v.cov->SetBinContent(x,y,cCov+cTmp);
            }
        }

        if (!isMC) continue;

        for (auto& jpWgt: jpWgts) {

            if (jpWgt.iGen >= NGen) continue;
            GenJet& genJet = genJets->at(jpWgt.iGen);

            auto genpt = genJet.p4.Pt(),
                 geny = abs(genJet.Rapidity());
            //int genybin = geny*2; genybin = min(genybin,nYbins-1);
            
            auto igenbin = genBinning->GetGlobalBinNumber(genpt,geny);

            bool LowGenPt  = genpt < minpt,
                 HighGenPt = genpt >= maxpt/*s.at(genybin)*/,
                 HighGenY  =  geny >= maxy;
            bool goodGen = (!LowGenPt) && (!HighGenPt) && (!HighGenY);

            bool noMatch = jpWgt.iRec >= NRec;

            for (Variation& v: variations) {

                auto evWgt = ev->weights.at(v.iEvtWgt);

                if (goodGen) v.gen->Fill(igenbin, evWgt * jpWgt.weight);

                if (noMatch) {
                    if (goodGen) {
                        if (geny < 1.3) v.missNoMatchBB->Fill(igenbin, evWgt * jpWgt.weight);
                        else            v.missNoMatchEC->Fill(igenbin, evWgt * jpWgt.weight);
                    }
                    continue;
                }
                RecJet& recJet = recJets->at(jpWgt.iRec);

                auto jWgt = recJet.weights.at(v.iJetWgt); 

                auto recpt = recJet.CorrPt(v.iJEC),
                     recy  = abs(recJet.Rapidity());
                //int recybin = recy*2; recybin = min(recybin,nYbins-1);

                bool LowRecPt  = recpt < minpt,
                     HighRecPt = recpt >= maxpt/*s.at(recybin)*/,
                     HighRecY  =  recy >= maxy;
                bool goodRec = (!LowRecPt) && (!HighRecPt) && (!HighRecY);

                auto irecbin = recBinning->GetGlobalBinNumber(recpt,recy);

                if      ( goodRec &&  goodGen) {                                                 v.RM           ->Fill(igenbin, irecbin, evWgt * jpWgt.weight *      jWgt );
                                                                                 if (geny < 1.3) v.missNoMatchBB->Fill(igenbin,          evWgt * jpWgt.weight * (1 - jWgt));   // for jet weights != 1 
                                                                                 else            v.missNoMatchEC->Fill(igenbin,          evWgt * jpWgt.weight * (1 - jWgt)); } // for jet weights != 1 
                else if (!goodRec &&  goodGen) { if (  LowRecPt  && (!HighRecPt) && (!HighRecY)) v.missOutLow   ->Fill(igenbin,          evWgt * jpWgt.weight             );
                                            else if ((!LowRecPt) &&   HighRecPt  && (!HighRecY)) v.missOutHigh  ->Fill(igenbin,          evWgt * jpWgt.weight             );
                                            else if ((!LowRecPt) && (!HighRecPt) &&   HighRecY ) v.missOutY     ->Fill(igenbin,          evWgt * jpWgt.weight             ); 
                                            else                                                 v.missNoMatchEC->Fill(igenbin,          evWgt * jpWgt.weight             ); }
                else if ( goodRec && !goodGen) { if (  LowGenPt  && (!HighGenPt) && (!HighGenY)) v.fakeOutLow   ->Fill(         irecbin, evWgt * jpWgt.weight *      jWgt );
                                            else if ((!LowGenPt) &&   HighGenPt  && (!HighGenY)) v.fakeOutHigh  ->Fill(         irecbin, evWgt * jpWgt.weight *      jWgt );
                                            else if ((!LowGenPt) && (!HighGenPt) &&   HighGenY ) v.fakeOutY     ->Fill(         irecbin, evWgt * jpWgt.weight *      jWgt );
                                            else                                                 v.fakeNoMatchEC->Fill(         irecbin, evWgt * jpWgt.weight *      jWgt ); }
            }
        }

        for (auto& jpWgt: jpWgts) {

            if (jpWgt.iGen < NGen) continue;
            if (jpWgt.iRec >= NRec) continue;
            RecJet& recJet = recJets->at(jpWgt.iRec);

            for (Variation& v: variations) {

                auto y  = abs(recJet.Rapidity());
                if (y >= maxy) continue;
                //int ybin = y*2; ybin = min(ybin,nYbins-1);
                auto pt = recJet.CorrPt(v.iJEC);
                if (pt < minpt || pt > maxpt/*s.at(ybin)*/) continue;

                auto evWgt = ev->weights.at(v.iEvtWgt);
                auto jWgt = recJet.weights.at(v.iJetWgt);

                auto irecbin = recBinning->GetGlobalBinNumber(pt,y);
                if (y < 1.3) v.fakeNoMatchBB->Fill(irecbin, evWgt * jpWgt.weight * jWgt);
                else         v.fakeNoMatchEC->Fill(irecbin, evWgt * jpWgt.weight * jWgt);
            }
        }
    }

    for (Variation& v: variations)
        v.Write(file);
    file->Close();
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    TH1::SetDefaultSumw2();
    gROOT->SetBatch();

    if (argc < 4) {
        cout << argv[0] << " input output model [nSplit [nNow]]\n"
             << "\twhere\tinput = any n-tuple\n"
             << "\t     \toutput = a root file with format compatible with `unfold` executable\n"
             << "\t     \tmodel = output of `getModelReweighting` (applied only if file exists & if MC)\n"
             << flush;
        return EXIT_SUCCESS;
    }

    TString input = argv[1],
            output = argv[2],
            model = argv[3];
    int nNow = 0, nSplit = 1;
    if (argc > 4) nSplit = atoi(argv[4]);
    if (argc > 5) nNow = atoi(argv[5]);

    getUnfHistogramsPtY(input, output, model, nSplit, nNow);
    return EXIT_SUCCESS;
}
#endif
