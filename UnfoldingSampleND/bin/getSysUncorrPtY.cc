#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>

#include <TROOT.h>
#include <TString.h>
#include <TFile.h>
#include <TH1D.h>
#include <TKey.h>

#include "Math/VectorUtil.h"

#include "PtY.h"

using namespace std;
using namespace DAS;
using namespace DAS::InclusiveJet;

////////////////////////////////////////////////////////////////////////////////
/// getSysUncorrPtY
void getSysUncorrPtY
             (TString input,  //!< name of input root file (data after `getUnfHistogramsPtY`)
              TString output) //!< name of output root file (one of the optional inputs of `unfold`)
{
    InitBinning();

    TFile * source = TFile::Open(input, "READ");
    TFile * file = TFile::Open(output, "RECREATE");

    TIter Next(source->GetListOfKeys()) ;
    TKey* key = nullptr;
    while ( (key = dynamic_cast<TKey*>(Next())) ) {

        source->cd();
        auto dirIn = dynamic_cast<TDirectory*>(key->ReadObj());
        TString var = dirIn->GetName(); // data variation name
        cerr << "\e[1m" << var << "\e[0m\n";
        dirIn->cd();

        TH1 * rec = dynamic_cast<TH1*>(dirIn->Get("rec"));
        assert(rec != nullptr);
        rec->SetDirectory(0);

        file->cd();
        auto dirOut = file->mkdir(var);
        dirOut->cd();

        TString title = "bin-to-bin uncorrelated systematic uncertainty";
        TH1 * sysUncorr1D = recBinning->CreateHistogram           ("sysUncorr1D", false, 0, title + " 1D");
        TH2 * sysUncorr2D = recBinning->CreateErrorMatrixHistogram("sysUncorr2D", false, 0, title + " 2D");

        for (int iy = 1; iy <= nYbins; ++iy)
        for (int ipt = 1; ipt <= nRecBins; ++ipt) {
            double y = (y_edges.at(iy-1) + y_edges.at(iy)) / 2,
                   pt = (recBins.at(ipt-1) + recBins.at(ipt)) / 2;
            int i = recBinning->GetGlobalBinNumber(pt, y);

            double content = rec->GetBinContent(i),
                   factor  = iy == nYbins ? 0.01 : 0.002;
            sysUncorr1D->SetBinError  (i,       content*factor   );
            sysUncorr2D->SetBinContent(i,i, pow(content*factor,2));
        }

        sysUncorr1D->SetDirectory(dirOut);
        sysUncorr1D->Write();

        sysUncorr2D->SetDirectory(dirOut);
        sysUncorr2D->Write();
    }

    source->Close();
    file->Close();
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    TH1::SetDefaultSumw2();
    gROOT->SetBatch();

    if (argc < 3) {
        cout << argv[0] << " input output\n"
             << "\twhere\tinput = data from `getUnfHistogramsPtY`\n"
             << "\t     \toutput = root file to be used as last-but-one argument of `unfold`\n"
             << flush;
        return EXIT_SUCCESS;
    }

    TString input = argv[1],
            output = argv[2];

    getSysUncorrPtY(input, output);
    return EXIT_SUCCESS;
}
#endif


