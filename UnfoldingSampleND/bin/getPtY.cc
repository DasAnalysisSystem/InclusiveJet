#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>

#include <TROOT.h>
#include <TString.h>
#include <TFile.h>
#include <TH1D.h>
#include <TKey.h>

#include "Math/VectorUtil.h"
#include "PtY.h"

using namespace std;
using namespace DAS;
using namespace DAS::InclusiveJet;

////////////////////////////////////////////////////////////////////////////////
/// getPtY
///
/// Extract with TUnfoldBinning proper TH2 plain-ROOT histograms
/// from the mappings to TH1 histograms 
///
/// Also calculate uncertainties for plotting
void getPtY 
             (TString input,  //!< name of input root file 
              TString output, //!< name of output root file
              TString alternative = "", //!< alternative to input file for model uncertainty
              TString smooth = "") //!< alternative to input file for smooth systematics
{
    const float relTrigUnc = 0.0; // %
    const float prefUnc = 0.0;

    InitBinning();

    TFile * source = TFile::Open(input, "READ");

    TH1 * unf      = dynamic_cast<TH1 *>(source->Get("nominal" )); 
    TH2 * cov      = dynamic_cast<TH2 *>(source->Get("cov"     ));
    TH2 * covTotal = dynamic_cast<TH2 *>(source->Get("covTotal"));
    unf     ->SetDirectory(0); 
    cov     ->SetDirectory(0);
    covTotal->SetDirectory(0);

    const int N = unf->GetNbinsX();
    
    if (relTrigUnc > 0) {
        cout << "Adding bin-to-bin trigger uncertainty of " << relTrigUnc << "%" << endl;
        for (int i = 1; i <= N; ++i) {
            double content = unf->GetBinContent(i),
                   error2  = covTotal->GetBinContent(i,i);
            double unc = i > 4*N/5 ? prefUnc : relTrigUnc;
            error2 += pow( (unc/100) * content, 2);
            covTotal->SetBinContent(i,i,error2);
        }
    }

    unf->SetName("unf");

    unf->SetLineColor(kBlack);
    unf->SetMarkerColor(kBlack);

    TH1 * unfUncorr = dynamic_cast<TH1 *>(unf->Clone("unfUncorr"));

    // STATISTICAL UNCERTAINTIES
    for (int i = 1; i <= N; ++i) {
        double content = unf->GetBinContent(i),
               err2    = cov->GetBinContent(i,i),
               errTot2 = covTotal->GetBinContent(i,i);
        if (content > 0) {
            unf      ->SetBinError(i, sqrt(err2   ));
            unfUncorr->SetBinError(i, sqrt(errTot2));
        }
    }

    TFile * sourceSmooth = smooth != "" ? TFile::Open(smooth, "READ") : source;

    // JES UNCERTAINTIES
    TH1 * JESup = dynamic_cast<TH1*>(unf->Clone("JESup")),
        * JESdn = dynamic_cast<TH1*>(unf->Clone("JESdn"));
    {
        TDirectory * dJES = dynamic_cast<TDirectory *>(sourceSmooth->Get("JES"));
        dJES->cd();
        TIter Next(dJES->GetListOfKeys()) ;
        TKey* key = nullptr;
        JESup->Reset();
        JESdn->Reset();
        while ( (key = dynamic_cast<TKey*>(Next())) ) {
            TH1 * h = dynamic_cast<TH1*>(key->ReadObj());
            if (h == nullptr) continue;
            TString name = h->GetName();
            if (name.Contains("Pref")) continue;
            h->Add(unf, -1);
            //if (name.Contains("FlavorQCD")) h->Scale(2);
            for (int i = 1; i <= N; ++i) {
                double content = h->GetBinContent(i);
                TH1 * var = (content > 0) ? JESup : JESdn;
                double content2 = var->GetBinContent(i);
                var->SetBinContent(i, hypot(content, content2));
            }
        }
        JESdn->Scale(-1);
        JESup->Add(unf);
        JESdn->Add(unf);
        JESup->SetMarkerColor(kRed-7);
        JESdn->SetMarkerColor(kRed-7);
        JESup->SetLineColor(kRed-7);
        JESdn->SetLineColor(kRed-7);
        JESup->SetFillColor(kRed-7);
        JESdn->SetFillColor(kRed-7);
    }

    // OTHER UNCERTAINTIES 
    TH1 * OTHERup = dynamic_cast<TH1*>(unf->Clone("OTHERup")),
        * OTHERdn = dynamic_cast<TH1*>(unf->Clone("OTHERdn"));
    {
        TDirectory * dMC = dynamic_cast<TDirectory *>(sourceSmooth->Get("MC"));
        dMC->cd();

        // lumi
        OTHERup->Scale(0.012);
        OTHERdn->Scale(0.012);

        // model
        if (alternative != "") {
            cout << "Including model uncertainty (also made symmetric)" << endl;
            TFile * altSource = TFile::Open(alternative, "READ");
            TH1 * model = dynamic_cast<TH1 *>(altSource->Get("nominal")); 
            model->SetName("model");
            model->SetDirectory(0);
            altSource->Close();
            source->cd();
            for (int i = 1; i <= model->GetNbinsX(); ++i) {
                double nom = unf->GetBinContent(i),
                       var = model->GetBinContent(i),
                       diff = nom - var;
                //TH1 * other = diff > 0 ? OTHERup : OTHERdn;
                //double content = other->GetBinContent(i);
                //content = hypot(content, diff);
                //other->SetBinContent(i, content);
                double contentup = OTHERup->GetBinContent(i);
                double contentdn = OTHERdn->GetBinContent(i);
                contentup = hypot(contentup, diff);
                contentdn = hypot(contentdn, diff);
                OTHERup->SetBinContent(i, contentup);
                OTHERdn->SetBinContent(i, contentdn);
            }
        }
        else
            cout << "**NOT** including model uncertainty" << endl;

        // miss & fake (and pref, if it is done in the MC)
        TIter Next(dMC->GetListOfKeys()) ;
        TKey* key = nullptr;
        while ( (key = dynamic_cast<TKey*>(Next())) ) {
            TH1 * h = dynamic_cast<TH1*>(key->ReadObj());
            if (h == nullptr) continue;
            TString name = h->GetName();
            if (name.BeginsWith("JER")/* || name.Contains("Model")*/) continue;
            if (smooth != "" || (!name.Contains("miss") && !name.Contains("fake"))) h->Add(unf,-1);
            cout << '\t' << name << '\n';
            for (int i = 1; i <= N; ++i) {
                double content = h->GetBinContent(i);
                TH1 * var = (content > 0) ? OTHERup : OTHERdn;
                double content2 = var->GetBinContent(i);
                var->SetBinContent(i, hypot(content, content2));
            }
        }

        // prefiring if it was done in the data instead of the MC
        TH1 * Prefup = dynamic_cast<TH1*>(gFile->Get("JES/Prefup"  )),
            * Prefdn = dynamic_cast<TH1*>(gFile->Get("JES/Prefdown"));
        if (Prefup == nullptr && Prefdn == nullptr) 
            cout << "Prefiring was already included in the MC" << endl;
        else {
            cout << "Prefiring included in the data" << endl;
            Prefup->Add(unf,-1);
            Prefdn->Add(unf,-1);
            for (auto h: {Prefup, Prefdn})
            for (int i = 1; i <= N; ++i) {
                double content = h->GetBinContent(i);
                TH1 * var = (content > 0) ? OTHERup : OTHERdn;
                double content2 = var->GetBinContent(i);
                var->SetBinContent(i, hypot(content, content2));
            }
        }


        OTHERdn->Scale(-1);
        OTHERup->Add(unf);
        OTHERdn->Add(unf);
        OTHERup->SetMarkerColor(kBlue-4);
        OTHERdn->SetMarkerColor(kBlue-4);
        OTHERup->SetLineColor  (kBlue-4);
        OTHERdn->SetLineColor  (kBlue-4);
        OTHERup->SetFillColor  (kBlue-4);
        OTHERdn->SetFillColor  (kBlue-4);
    }

    // JER UNCERTAINTIES 
    TH1 * JERup = dynamic_cast<TH1*>(sourceSmooth->Get("MC/JERup"  )),
        * JERdn = dynamic_cast<TH1*>(sourceSmooth->Get("MC/JERdown"));

    JERup->SetName("JERup");
    JERdn->SetName("JERdn");

    JERup->SetMarkerColor(kYellow-6);
    JERdn->SetMarkerColor(kYellow-6);
    JERup->SetLineColor  (kYellow-6);
    JERdn->SetLineColor  (kYellow-6);
    JERup->SetFillColor  (kYellow-6);
    JERdn->SetFillColor  (kYellow-6);

    // TOTAL UNCERTAINTIES 
    TH1 * TOTALup = dynamic_cast<TH1*>(unf->Clone("TOTALup")),
        * TOTALdn = dynamic_cast<TH1*>(unf->Clone("TOTALdn"));
    TOTALup->SetLineColor(kGreen+2);
    TOTALdn->SetLineColor(kGreen+2);
    TOTALup->SetMarkerColor(kGreen+2);
    TOTALdn->SetMarkerColor(kGreen+2);

    for (int i = 1; i <= N; ++i) {
        double u    = unf->GetBinContent(i), a2 = covTotal->GetBinContent(i,i),
               b_up = JESup->GetBinContent(i), b_dn = JESdn->GetBinContent(i),
               c_up = JERup->GetBinContent(i), c_dn = JERdn->GetBinContent(i),
               d_up = OTHERup->GetBinContent(i), d_dn = OTHERdn->GetBinContent(i);

        if (b_up < b_dn) swap(b_up, b_dn);
        if (c_up < c_dn) swap(c_up, c_dn);
        if (d_up < d_dn) swap(d_up, d_dn);

        JESup  ->SetBinContent(i,b_up); JESdn  ->SetBinContent(i,b_dn);
        JERup  ->SetBinContent(i,c_up); JERdn  ->SetBinContent(i,c_dn);
        OTHERup->SetBinContent(i,d_up); OTHERdn->SetBinContent(i,d_dn);

        b_up -= u; b_dn -= u;
        c_up -= u; c_dn -= u;
        d_up -= u; d_dn -= u;

        double sh_up = sqrt(a2 + pow(b_up,2) + pow(c_up,2) + pow(d_up,2)),
               sh_dn = sqrt(a2 + pow(b_dn,2) + pow(c_dn,2) + pow(d_dn,2));

        double a = sqrt(a2);

        cout << setw(3) << i 
             << setw(18) <<     u << setw(15) <<     a
             << setw(18) <<  b_up << setw(15) <<  b_dn 
             << setw(18) <<  c_up << setw(15) <<  c_dn 
             << setw(18) <<  d_up << setw(15) <<  d_dn
             << setw(18) << sh_up << setw(15) << sh_dn << '\n';

        assert(sh_up >= a);
        assert(sh_dn >= a);

        TOTALup->SetBinContent(i,u+sh_up);
        TOTALdn->SetBinContent(i,u-sh_dn);
    }
    cout << flush;

    JESup->SetTitle("JES");
    JESdn->SetTitle("");
    JERup->SetTitle("JER");
    JERdn->SetTitle("");
    OTHERup->SetTitle("Other");
    OTHERdn->SetTitle("");
    TOTALup->SetTitle("Total");
    TOTALdn->SetTitle("");
    unf->SetTitle("stat. unc.");
    unfUncorr->SetTitle("all uncorr. unc.");

    for (TH1 * h: { JESup, JESdn, JERup, JERdn, OTHERup, OTHERdn,
                        TOTALup, TOTALdn, unf, unfUncorr} )
        h->SetDirectory(0);

    source->Close();
    sourceSmooth->Close();

    TFile * file = TFile::Open(output, "RECREATE");

    for (TH1 * h: { JESup, JESdn, JERup, JERdn, OTHERup, OTHERdn,
                        TOTALup, TOTALdn, unf, unfUncorr} ) {

        h->Write();

        const char * name = Form("%s2D",h->GetName());
        TH2 * h2 = dynamic_cast<TH2*>(genBinning->ExtractHistogram(name, h));
        h2->SetTitle(h->GetTitle());
        h2->SetMarkerColor(h->GetMarkerColor());
        h2->SetLineColor(h->GetLineColor());
        h2->SetFillColor(h->GetFillColor());
        h2->Write();

        for (int y = 1; y <= nYbins; ++y) {
            const char * name_ybin = Form("%s_ybin%d",h2->GetName(), y);
            TH1 * h1 = h2->ProjectionX(name_ybin, y, y);
            h1->Write();
        }
    }

    cov     ->Write("cov"     );
    covTotal->Write("covTotal");

    file->Close();
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    TH1::SetDefaultSumw2();
    gROOT->SetBatch();

    if (argc < 3) {
        cout << argv[0] << " input output alternative\n"
             << "\twhere\tinput = root file, corresponding to output from `unfold` corresponding to 2D inclusive jet spectrum\n"
             << "\t     \toutput = root file with curves in proper binning \n"
             << "\t     \talternative = alternative to input used to derive model uncertainty (not mandatory)\n"
             << "\t     \tsmooth = alternative to input used to get smooth systematic uncertainties (not mandatory)\n"
             << flush;
        return EXIT_SUCCESS;
    }

    TString input = argv[1],
            output = argv[2],
            alternative = argc > 3 ? argv[3] : "",
            smooth = argc > 4 ? argv[4] : "";

    getPtY(input, output, alternative, smooth);
    return EXIT_SUCCESS;
}
#endif

