# BEGIN PLOT /CMS_2020_PAS_SMP_20_011/d*
LegendXPos=0.65
LegendYPos=0.95
# END PLOT

# BEGIN PLOT /CMS_2020_PAS_SMP_20_011/d01-x01-y01
Title=CMS, $\sqrt(s)$ = 13 TeV, Inclusive AK4 jets, $0.0 < |y| < 0.5$
XLabel=$p_{T}$ [GeV]
YLabel=d$^2\sigma$/d$p_{T}$d$y$ [pb/GeV]
FullRange=1
LogY=1
LogX=1
LegendYPos=0.95
RatioPlotYMax=1.4
RatioPlotYMin=0.6
# END PLOT

# BEGIN PLOT /CMS_2020_PAS_SMP_20_011/d02-x01-y01
Title=CMS, $\sqrt(s)$ = 13 TeV, Inclusive AK4 jets, $0.5 < |y| < 1.0$
XLabel=$p_{T}$ [GeV]
YLabel=d$^2\sigma$/d$p_{T}$d$y$ [pb/GeV]
FullRange=1
LogY=1
LogX=1
LegendYPos=0.95
RatioPlotYMax=1.4
RatioPlotYMin=0.6
# END PLOT

# BEGIN PLOT /CMS_2020_PAS_SMP_20_011/d03-x01-y01
Title=CMS, $\sqrt(s)$ = 13 TeV, Inclusive AK4 jets, $1.0 < |y| < 1.5$
XLabel=$p_{T}$ [GeV]
YLabel=d$^2\sigma$/d$p_{T}$d$y$ [pb/GeV]
FullRange=1
LogY=1
LogX=1
LegendYPos=0.96
RatioPlotYMax=1.4
RatioPlotYMin=0.6
# END PLOT

# BEGIN PLOT /CMS_2020_PAS_SMP_20_011/d04-x01-y01
Title=CMS, $\sqrt(s)$ = 13 TeV, Inclusive AK4 jets, $1.5 < |y| < 2.0$
XLabel=$p_{T}$ [GeV]
YLabel=d$^2\sigma$/d$p_{T}$d$y$ [pb/GeV]
FullRange=1
LogY=1
LogX=1
LegendYPos=0.96
RatioPlotYMax=1.4
RatioPlotYMin=0.6
# END PLOT



# BEGIN PLOT /CMS_2020_PAS_SMP_20_011/d21-x01-y01
Title=CMS, $\sqrt(s)$ = 13 TeV, Inclusive AK7 jets, $0.0 < |y| < 0.5$
XLabel=$p_{T}$ [GeV]
YLabel=d$^2\sigma$/d$p_{T}$d$y$ [pb/GeV]
FullRange=1
LogY=1
LogX=1
LegendYPos=0.95
RatioPlotYMax=1.4
RatioPlotYMin=0.6
# END PLOT

# BEGIN PLOT /CMS_2020_PAS_SMP_20_011/d22-x01-y01
Title=CMS, $\sqrt(s)$ = 13 TeV, Inclusive AK7 jets, $0.5 < |y| < 1.0$
XLabel=$p_{T}$ [GeV]
YLabel=d$^2\sigma$/d$p_{T}$d$y$ [pb/GeV]
FullRange=1
LogY=1
LogX=1
LegendYPos=0.95
RatioPlotYMax=1.4
RatioPlotYMin=0.6
# END PLOT

# BEGIN PLOT /CMS_2020_PAS_SMP_20_011/d23-x01-y01
Title=CMS, $\sqrt(s)$ = 13 TeV, Inclusive AK7 jets, $1.0 < |y| < 1.5$
XLabel=$p_{T}$ [GeV]
YLabel=d$^2\sigma$/d$p_{T}$d$y$ [pb/GeV]
FullRange=1
LogY=1
LogX=1
LegendYPos=0.95
RatioPlotYMax=1.4
RatioPlotYMin=0.6
# END PLOT

# BEGIN PLOT /CMS_2020_PAS_SMP_20_011/d24-x01-y01
Title=CMS, $\sqrt(s)$ = 13 TeV, Inclusive AK7 jets, $1.5 < |y| < 2.0$
XLabel=$p_{T}$ [GeV]
YLabel=d$^2\sigma$/d$p_{T}$d$y$ [pb/GeV]
FullRange=1
LogY=1
LogX=1
LegendYPos=0.95
RatioPlotYMax=1.4
RatioPlotYMin=0.6
# END PLOT

